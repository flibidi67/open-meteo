<?php

namespace Flibidi67\OpenMeteo;

use Flibidi67\OpenMeteo\DependencyInjection\Flibidi67OpenMeteoExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

final class Flibidi67OpenMeteoBundle extends AbstractBundle {

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new Flibidi67OpenMeteoExtension();
    }
}