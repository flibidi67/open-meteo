<?php

namespace Flibidi67\OpenMeteo\Service;

use Exception;
use Flibidi67\OpenMeteo\Object\MeteoFrance\Daily;
use Flibidi67\OpenMeteo\Object\MeteoFrance\Hourly;
use Flibidi67\OpenMeteo\Object\Settings;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MeteoFranceService extends AbstractOpenMeteoService {

    protected string $apiUrl = "https://api.open-meteo.com/v1/meteofrance";
    protected string $apiName = "meteofrance";

    /**
     * The constructor
     * @param HttpClientInterface $client
     * @param ParameterBagInterface $parameters
     */
    public function __construct(HttpClientInterface $client, ParameterBagInterface $parameters) {
        $this->client = $client;
        $this->hourly = new Hourly($this);
        $this->daily = new Daily($this);
        $this->settings = new Settings($this);
        parent::__construct($parameters);
    }

    /**
     * Return the daily object.
     * @return Daily
     */
    public function getDaily(): ?Daily {
        return $this->daily;
    }

    /**
     * Return the hourly object.
     * @return Hourly
     */
    public function getHourly(): Hourly {
        return $this->hourly;
    }
}