<?php

namespace Flibidi67\OpenMeteo\Service;

use Flibidi67\OpenMeteo\Object\Historical\Daily;
use Flibidi67\OpenMeteo\Object\Historical\Hourly;
use Flibidi67\OpenMeteo\Object\Settings;
use PHPUnit\Logging\Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HistoricalService extends AbstractOpenMeteoService {

    protected string $apiUrl = "https://archive-api.open-meteo.com/v1/archive";
    protected string $apiName = "historical";

    /**
     * The constructor
     * @param HttpClientInterface $client
     * @param ParameterBagInterface $parameters
     */
    public function __construct(HttpClientInterface $client, ParameterBagInterface $parameters) {
        $this->client = $client;
        $this->hourly = new Hourly($this);
        $this->daily = new Daily($this);
        $this->settings = new Settings($this);
        parent::__construct($parameters);
    }

    protected function extraValidation(): void {
        if (!$this->getSettings()->getStartDate() || !$this->getSettings()->getEndDate()) {
            throw new Exception("start_date and end_date settings are mandatory.");
        }
    }

    /**
     * Return the daily object.
     * @return Daily
     */
    public function getDaily(): ?Daily {
        return $this->daily;
    }

    /**
     * Return the hourly object.
     * @return Hourly
     */
    public function getHourly(): Hourly {
        return $this->hourly;
    }
}