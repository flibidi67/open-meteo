<?php

namespace Flibidi67\OpenMeteo\Service;

use Exception;
use Flibidi67\OpenMeteo\Object\DWD\Daily;
use Flibidi67\OpenMeteo\Object\DWD\Hourly;
use Flibidi67\OpenMeteo\Object\DWD\Minutely15;
use Flibidi67\OpenMeteo\Object\Settings;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DWDService extends AbstractOpenMeteoService {

    protected string $apiUrl = "https://api.open-meteo.com/v1/dwd-icon";
    protected string $apiName = "dwd";
    protected Minutely15 $minutely;

    /**
     * The constructor
     * @param HttpClientInterface $client
     * @param ParameterBagInterface $parameters
     */
    public function __construct(HttpClientInterface $client, ParameterBagInterface $parameters) {
        $this->client = $client;
        $this->hourly = new Hourly($this);
        $this->daily = new Daily($this);
        $this->settings = new Settings($this);
        $this->minutely = new Minutely15($this);
        parent::__construct($parameters);
    }

    /**
     * Return the daily object.
     * @return Daily
     */
    public function getDaily(): ?Daily {
        return $this->daily;
    }

    /**
     * Return the hourly object.
     * @return Hourly
     */
    public function getHourly(): Hourly {
        return $this->hourly;
    }

    /**
     * Return the Minutely object.
     * @return Hourly
     */
    public function getMinutely(): Minutely15 {
        return $this->minutely;
    }

    protected function buildQueryString(): string {
        return parent::buildQueryString() . $this->getMinutely()->toQueryString();
    }
}