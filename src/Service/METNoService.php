<?php

namespace Flibidi67\OpenMeteo\Service;

use Flibidi67\OpenMeteo\Object\AbstractDaily;
use Flibidi67\OpenMeteo\Object\METNo\Hourly;
use Flibidi67\OpenMeteo\Object\Settings;
use PHPUnit\Logging\Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class METNoService extends AbstractOpenMeteoService {

    protected string $apiUrl = "https://api.open-meteo.com/v1/metno";
    protected string $apiName = "metno";

    /**
     * The constructor
     * @param HttpClientInterface $client
     * @param ParameterBagInterface $parameters
     */
    public function __construct(HttpClientInterface $client, ParameterBagInterface $parameters) {
        $this->client = $client;
        $this->hourly = new Hourly($this);
        $this->settings = new Settings($this);
        parent::__construct($parameters);
    }

    /**
     * Return the daily object.
     * @return Daily
     */
    public function getDaily(): ?AbstractDaily {
        return $this->daily;
    }

    /**
     * Return the hourly object.
     * @return Hourly
     */
    public function getHourly(): Hourly {
        return $this->hourly;
    }
}