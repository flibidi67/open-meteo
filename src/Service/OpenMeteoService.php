<?php

namespace Flibidi67\OpenMeteo\Service;

use Exception;
use Flibidi67\OpenMeteo\Object\Daily;
use Flibidi67\OpenMeteo\Object\Hourly;
use Flibidi67\OpenMeteo\Object\Settings;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OpenMeteoService {

    const API_URL = "https://api.open-meteo.com/v1/forecast";

    // LES PARAMS METEO FRANCE AU CAS OU.
    const METEO_FRANCE_PARAMETERS = [
        "temperature_2m",
        "relativehumidity_2m",
        "dewpoint_2m",
        "apparent_temperature",
        "pressure_msl",
        "surface_pressure",
        "precipitation",
        "snowfall",
        "weathercode",
        "cloudcover",
        "cloudcover_low",
        "cloudcover_mid",
        "cloudcover_high",
        "et0_fao_evapotranspiration",
        "vapor_pressure_deficit",
        "cape",
        "windspeed_10m",
        "winddirection_10m",
        "windgusts_10m",
        "shortwave_radiation",
        "direct_radiation",
        "diffuse_radiation",
        "direct_normal_irradiance",
        "terrestrial_radiation",
        "shortwave_radiation_instant",
        "direct_radiation_instant",
        "diffuse_radiation_instant",
        "direct_normal_irradiance_instant",
        "terrestrial_radiation_instant",
        "temperature_1000hPa",
        "temperature_950hPa",
        "temperature_900hPa",
        "temperature_700hPa",
        "temperature_650hPa",
        "temperature_600hPa",
    ];

    private HttpClientInterface $client;
    private float $longitude;
    private float $latitude;
    private array $parameters = [];
    private Hourly $hourly;
    private Daily $daily;
    private Settings $settings;
    private ?string $url = null;

    /**
     * The constructor
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client) {
        $this->client = $client;
        $this->hourly = new Hourly();
        $this->daily = new Daily();
        $this->settings = new Settings();
    }

    /**
     * Set the coordinates for the query.
     * @param float $longitude
     * @param float $latitude
     * @return self
     */
    public function setCoordinates(float $longitude, float $latitude): self {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * Add $parameter with $value to $this->parameters array.
     * @param string $parameter
     * @param mixed $value
     * @return self
     */
    public function with(string $parameter, mixed $value = null): self {
        if (is_null($value)) {
            if (in_array($parameter, $this->hourly->getAvailablesParams())) {
                $this->hourly->with($parameter);
            }
            //TODO: THROW ERROR ?
            return $this;
        }

        $this->parameters[$parameter] = $value;
        return $this;
    }

    /**
     * Build the final URL.
     * @return string
     * @throws Exception
     */
    private function buildUrl(): string {
        if (!$this->latitude || !$this->longitude) {
            throw new Exception("Latitude and longiture are mandatory.");
        }

        if (!empty($this->getHourly()->getParameters() && is_null($this->getSettings()->getTimezone()))) {
            throw new Exception("Timezone is mandatory when daily parameters are set.");
        }

        $queryString = "?latitude=" . $this->latitude . "&longitude=" . $this->longitude .
                $this->getHourly()->toQueryString() .
                $this->getDaily()->toQueryString() .
                $this->getSettings()->toQueryString();
        foreach ($this->parameters as $key => $value) {
            if (is_array($value)) {
                $value = implode(",", $value);
            }
            $queryString .= (empty($queryString) ? "?" : "&") . "$key=$value";
        }
        return self::API_URL . $queryString;
    }

    /**
     * Call the API and return the content as JSON.
     * @param bool $format, if true, then use the formatter defined for the service and return formatted data.
     * @throws Exception
     * @return array
     */
    public function get(bool $format = false): array {
        $response = $this->client->request(
            'GET',
            $this->url ?? $this->buildUrl()
        )->getContent(false);

        $json = json_decode($response, true);
        if (isset($json['error'])) {
            throw new Exception($json['reason']);
        }

        return $json;
    }

    /**
     * Set the URL.
     * @param ?string $url
     * @return self
     */
    public function setUrl(?string $url): self {
        $this->url = $url;
        return $this;
    }

    /**
     * Return $this->hourly
     * @return Hourly
     */
    public function getHourly(): Hourly {
        return $this->hourly;
    }

    /**
     * Return $this->daily
     * @return Daily
     */
    public function getDaily(): Daily {
        return $this->daily;
    }

    /**
     * Return $this->settings
     * @return Settings
     */
    public function getSettings(): Settings {
        return $this->settings;
    }

    /**
     * Magic call to simplify life
     * withHourlyTemperature2m for example.
     * withSettingsTimeformat("unixtime")
     * @return self
     */
    public function __call($method, $arguments) {
        if (strpos(strtolower($method), "withhourly") !== false) {
            $method = "with" . ucfirst(substr($method, strlen("withHourly")));
            $this->getHourly()->$method($arguments);
        } elseif (strpos(strtolower($method), "withdaily") !== false) {
            $method = "with" . ucfirst(substr($method, strlen("withDaily")));
            $this->getDaily()->$method($arguments);
        } elseif (strpos(strtolower($method), "withsettings") !== false) {
            $method = "set" . ucfirst(substr($method, strlen("withSettings")));
            $this->getSettings()->$method($arguments);
        }
        return $this;
    }
}