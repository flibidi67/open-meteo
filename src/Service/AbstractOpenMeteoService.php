<?php

namespace Flibidi67\OpenMeteo\Service;

use Exception;
use Flibidi67\OpenMeteo\Contract\FormatterInterface;
use Flibidi67\OpenMeteo\Formatter\DefaultFormatter;
use Flibidi67\OpenMeteo\Object\AbstractDaily;
use Flibidi67\OpenMeteo\Object\AbstractHourly;
use Flibidi67\OpenMeteo\Object\Daily;
use Flibidi67\OpenMeteo\Object\Hourly;
use Flibidi67\OpenMeteo\Object\Settings;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractOpenMeteoService {

    protected ?string $url = null;
    protected string $apiUrl;
    protected string $apiName;
    protected ?float $longitude;
    protected ?float $latitude;
    protected array $parameters = [];
    protected AbstractHourly $hourly;
    protected ?AbstractDaily $daily = null;
    protected Settings $settings;
    protected HttpClientInterface $client;
    protected ?FormatterInterface $formatter = null;

    /**
     * The constructor, call in extended class in order to set the Settings object with values define in the config file.
     * @param ParameterBagInterface $parameters
     */
    public function __construct(ParameterBagInterface $parameters) {
        $this->settings->setAll($parameters->get("flibidi67_open_meteo." . $this->apiName));
    }

    /**
     * Return $this->settings
     * @return Settings
     */
    public function getSettings(): Settings {
        return $this->settings;
    }

    /**
     * Return the latitude.
     * @return float|null
     */
    public function getLatitude(): ?float {
        return $this->latitude;
    }

    /**
     * Return the longitude.
     * @return float|null
     */
    public function getLongitude(): ?float {
        return $this->longitude;
    }

    /**
     * Set the coordinates for the query.
     * @param float $longitude
     * @param float $latitude
     * @return self
     */
    public function setCoordinates(float $longitude, float $latitude): self {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * Build the query string..
     * @return string
     * @throws Exception
     */
    protected function buildQueryString(): string {
        if (!$this->getLatitude() || !$this->getLongitude()) {
            throw new Exception("Latitude and longiture are mandatory.");
        }

        if ($this->getDaily() && (!empty($this->getDaily()->getParameters() && is_null($this->getSettings()->getTimezone())))) {
            throw new Exception("Timezone is mandatory when daily parameters are set.");
        }

        $this->extraValidation();

        $queryString = "?latitude=" . $this->latitude . "&longitude=" . $this->longitude .
            $this->getHourly()->toQueryString() .
            ($this->getDaily() ? $this->getDaily()->toQueryString() : '') .
            $this->getSettings()->toQueryString();
        foreach ($this->parameters as $key => $value) {
            if (is_array($value)) {
                $value = implode(",", $value);
            }
            $queryString .= (empty($queryString) ? "?" : "&") . "$key=$value";
        }

        return $queryString;
    }

    /**
     * Call the API and return the content as JSON.
     * @throws Exception
     * @return array
     */
    public function get(): array {
        $response = $this->client->request(
            'GET',
            $this->url ?? $this->apiUrl . $this->buildQueryString()
        )->getContent(false);

        $json = json_decode($response, true);
        if (isset($json['error'])) {
            throw new Exception($json['reason']);
        }

        return $this->formatter ? $this->formatter->format($json) : $json;
    }

    /**
     * Set the URL.
     * @param ?string $url
     * @return self
     */
    public function setUrl(?string $url): self {
        $this->url = $url;
        return $this;
    }

    /**
     * Set the formatter to use.
     * @param ?FormatterInterface $formatter
     * @return self
     */
    public function useFormatter(?FormatterInterface $formatter): self {
        $this->formatter = $formatter;
        return $this;
    }

    /**
     * Set the default formatter.
     * @return self
     */
    public function useDefaultFormatter(): self {
        $this->useFormatter(new DefaultFormatter());
        return $this;
    }

    /**
     * To override in services that need an extra validation when building the querystring (eg: HistoricalService)
     * @return void
     */
    protected function extraValidation(): void {}

    abstract public function getHourly(): AbstractHourly;
    abstract public function getDaily(): ?AbstractDaily;
}
