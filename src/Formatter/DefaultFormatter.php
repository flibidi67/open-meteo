<?php

namespace Flibidi67\OpenMeteo\Formatter;

use DateTime;
use DateTimeZone;
use Dompdf\Exception;
use Flibidi67\OpenMeteo\Contract\FormatterInterface;
use Flibidi67\OpenMeteo\Object\Settings;

/**
 * This formatter return the data retrieving by the API in the same format EXCEPT for the hourly, daily and minutely_15
 * keys which are formatted in a associating array following this structure :
 * [
 *     'date in the format : Y-m-d' // and with hours and minutes for hourly and minutely_15
 *          => [
 *              'time' => a DateTime corresponding to the key
 *              'temperature_2m => value // for example
 *              // and all the other values presents in the querystring
 *             ]
 *      ...
 * ]
 *
 */
class DefaultFormatter implements FormatterInterface {
    public function format(array $data): array {
        $skip = ['hourly', 'hourly_units', 'daily', 'daily_units', 'minutely_15', 'minutely_15_units'];

        $res = [];
        foreach ($data as $key => $value) {
            if (in_array($key, $skip)) {
                continue;
            }
            $res[$key] = $value;
        }

        if (isset($data['hourly'])) {
            $res = array_merge($res, $this->formatDataForType('hourly', $data));
        }
        
        if (isset($data['daily'])) {
            $res = array_merge($res, $this->formatDataForType('daily', $data));
        }

        if (isset($data['minutely_15'])) {
            $res = array_merge($res, $this->formatDataForType('minutely_15', $data));
        }

        return $res;
    }

    /**
     * Format the data for a specific type (daily or hourly).
     * @param string $type
     * @param array $data
     * @return array
     */
    private function formatDataForType(string $type, array $data): array {
        $res = [
            $type => [],
            $type . "_units" => $data[$type . "_units"]
        ];

        $unixtime = $res[$type . '_units']['time'] === Settings::TIMEFORMAT_UNIX;
        $dateFormat = $type === "daily" ? 'Y-m-d' : 'Y-m-d\TH:i';
        $formatDateTo = $type === "daily" ? 'Y-m-d' : 'Y-m-d H:i';
        $dateCache = [];
        
        if (isset($data[$type])) {
            $res[$type] = [];
            $times = [];
            foreach ($data[$type]['time'] as $time) {
                $date = $unixtime ?
                    (new DateTime())->setTimestamp($time)->setTimezone(new DateTimeZone($data['timezone'])) :
                    (DateTime::createFromFormat($dateFormat, $time, new DateTimeZone($data['timezone'])));
                $key = $date->format($formatDateTo);
                $times[] = $key;
                $dateCache[$key] = $date;
            }

            unset($data[$type]['time']);

            for ($i = 0; $i < count($times); $i++) {
                $res[$type][$times[$i]] = [
                    'time' => $dateCache[$times[$i]]
                ];
                foreach ($data[$type] as $field => $values) {
                    $res[$type][$times[$i]][$field] = $values[$i];
                }
            }
        }
        
        return $res;
    }
}