<?php

namespace Flibidi67\OpenMeteo\Object;

class AbstractHourly extends AbstractObject {

    protected ?string $pressureLevelClass = null;
    protected array $pressureLevels = [];

    /**
     * Return the Hourly object as a querystring.
     * @return string
     */
    public function toQueryString(): string {
        if (!empty($this->parameters) || !empty($this->pressureLevels)) {
            $queryString = "&hourly=";
            if (!empty($this->parameters)) {
                $queryString .= implode(",", array_unique($this->parameters));
            }

            if ($this->pressureLevelClass && !empty($this->pressureLevels)) {
                if (!empty($this->parameters)) {
                    $queryString .= ",";
                }
                $i = 0;
                foreach ($this->pressureLevels as $pressureLevel) {
                    if (++$i) {
                        $queryString .= ",";
                    }
                    $queryString .= $pressureLevel->toQueryString();
                }
            }
            return $queryString;
        }
        return "";
    }

    /**
     * Add $parameter to $this->parameters.
     * @param string|array $parameter
     * @return self
     */
    public function with(string|array $parameter): self {
        if (is_string($parameter) && strpos($parameter, ",")) {
            $parameter = explode(',', $parameter);
        }
        if (is_array($parameter)) {
            $this->parameters = array_merge($this->parameters, $parameter);
        } else {
            $this->parameters[] = $parameter;
        }
        return $this;
    }

    /**
     * With all hourly parameters.
     * @param bool $withPressureLevels, if true, with all pressure levels..
     */
    public function withAll(bool $withPressureLevels = false): self {
        $this->parameters = array_values($this->availablesParameters);
        if ($this->pressureLevelClass && $withPressureLevels) {
            foreach ($this->pressureLevelClass::TYPES as $type) {
                foreach ($this->pressureLevelClass::AVAILABLES_PRESSURES as $pressure) {
                    $this->withPressureLevel($type, $pressure);
                }
            }
        }
        return $this;
    }

    /**
     * Add the $pressure for the pressure $type.
     * @param string $type
     * @param int $pressure
     * @return $this
     * @throws Exception if pressure type doesn't exists.
     */
    public function withPressureLevel(string $type, int $pressure): self {
        if ($this->pressureLevelClass) {
            if (!isset($this->pressureLevels[$type])) {
                $this->pressureLevels[$type] = new $this->pressureLevelClass($type);
            }
            $this->pressureLevels[$type]->withPressure($pressure);
        }
        return $this;
    }

    /**
     * Add the pressure level for temperature.
     * @param int $pressure
     * @return self
     */
    public function withPressureLevelTemperature(int $pressure): self {
        return $this->withPressureLevel(PressureLevel::TYPE_TEMPERATURE, $pressure);
    }

    /**
     * Add the pressure level for dewpoint.
     * @param int $pressure
     * @return self
     */
    public function withPressureLevelDewpoint(int $pressure): self {
        return $this->withPressureLevel(PressureLevel::TYPE_DEWPOINT, $pressure);
    }

    /**
     * Add the pressure level for relativehumidity.
     * @param int $pressure
     * @return self
     */
    public function withPressureLevelRelativeHumidity(int $pressure): self {
        return $this->withPressureLevel(PressureLevel::TYPE_RELATIVE_HUMIDITY, $pressure);
    }

    /**
     * Add the pressure level for cloud cover.
     * @param int $pressure
     * @return self
     */
    public function withPressureLevelCloudCover(int $pressure): self {
        return $this->withPressureLevel(PressureLevel::TYPE_CLOUD_COVER, $pressure);
    }

    /**
     * Add the pressure level for wind speed.
     * @param int $pressure
     * @return self
     */
    public function withPressureLevelWindSpeed(int $pressure): self {
        return $this->withPressureLevel(PressureLevel::TYPE_WIND_SPEED, $pressure);
    }

    /**
     * Add the pressure level for wind direction.
     * @param int $pressure
     * @return self
     */
    public function withPressureLevelWindDirection(int $pressure): self {
        return $this->withPressureLevel(PressureLevel::TYPE_WIND_DIRECTION, $pressure);
    }

    /**
     * Add the pressure level for geopotential height.
     * @param int $pressure
     * @return self
     */
    public function withPressureLevelGeopotentialHeight(int $pressure): self {
        return $this->withPressureLevel(PressureLevel::TYPE_GEOPOTENTIAL_HEIGHT, $pressure);
    }
}