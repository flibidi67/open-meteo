<?php

namespace Flibidi67\OpenMeteo\Object\DWD;

use Flibidi67\OpenMeteo\Object\AbstractPressureLevel;

class PressureLevel extends AbstractPressureLevel {

    const TYPES = [
        self::TYPE_TEMPERATURE,
        self::TYPE_DEWPOINT,
        self::TYPE_RELATIVE_HUMIDITY,
        self::TYPE_CLOUD_COVER,
        self::TYPE_WIND_SPEED,
        self::TYPE_WIND_DIRECTION,
        self::TYPE_GEOPOTENTIAL_HEIGHT,
    ];

    const AVAILABLES_PRESSURES = [
        30,
        50,
        70,
        100,
        150,
        200,
        250,
        300,
        400,
        500,
        600,
        700,
        800,
        850,
        900,
        925,
        950,
        975,
        1000
    ];

    private string $type;
    private array $parameters = [];

    protected function getTypes(): array {
        return self::TYPES;
    }

    protected function getAvailablesPressures(): array {
        return self::AVAILABLES_PRESSURES;
    }
}