<?php

namespace Flibidi67\OpenMeteo\Object\DWD;

use Flibidi67\OpenMeteo\Object\AbstractObject;
use Flibidi67\OpenMeteo\Traits\CAPE;
use Flibidi67\OpenMeteo\Traits\FreezingLevelHeight;
use Flibidi67\OpenMeteo\Traits\LightningPotential;
use Flibidi67\OpenMeteo\Traits\Precipitation\Precipitation;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradiance;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradianceInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Rain\Rain;
use Flibidi67\OpenMeteo\Traits\Snow\Snowfall;
use Flibidi67\OpenMeteo\Traits\Snow\SnowfallHeight;

class Minutely15 extends AbstractObject {
    use Precipitation, Rain, Snowfall, SnowfallHeight, FreezingLevelHeight, CAPE, ShortWaveRadiation, LightningPotential,
        DirectRadiation, DiffuseRadiation, DirectNormalIrradiance, TerrestrialRadiation, ShortWaveRadiationInstant,
        DirectRadiationInstant, DiffuseRadiationInstant, DirectNormalIrradianceInstant, TerrestrialRadiationInstant;

    protected array $availablesParameters = [
        self::PRECIPITATION,
        self::RAIN,
        self::SNOWFALL,
        self::SNOWFALL_HEIGHT,
        self::FREEZING_LEVEL_HEIGHT,
        self::CAPE,
        self::SHORT_WAVE_RADIATION,
        self::LIGHTNING_POTENTIAL,
        self::DIRECT_RADIATION,
        self::DIFFUSE_RADIATION,
        self::DIRECT_NORMAL_IRRADIANCE,
        self::TERRESTRIAL_RADIATION,
        self::SHORT_WAVE_RADIATION_INSTANT,
        self::DIRECT_RADIATION_INSTANT,
        self::DIFFUSE_RADIATION_INSTANT,
        self::DIRECT_NORMAL_IRRADIANCE_INSTANT,
        self::TERRESTRIAL_RADIATION_INSTANT
    ];

    /**
     * Return the Hourly object as a querystring.
     * @return string
     */
    public function toQueryString(): string {
        return empty($this->parameters) ? "" : "&minutely_15=" . implode(',', array_unique($this->parameters));
    }

    /**
     * Add $parameter to $this->parameters.
     * @param string|array $parameter
     * @return self
     */
    public function with(string|array $parameter): self {
        if (is_string($parameter) && strpos($parameter, ",")) {
            $parameter = explode(',', $parameter);
        }
        if (is_array($parameter)) {
            $this->parameters = array_merge($this->parameters, $parameter);
        } else {
            $this->parameters[] = $parameter;
        }
        return $this;
    }

    /**
     * With all minutely parameters.
     */
    public function withAll(): self {
        $this->parameters = array_values($this->availablesParameters);
        return $this;
    }

    public function getHourly(): Hourly {
        return $this->parent->getHourly();
    }

    public function getDaily(): ?Daily {
        return $this->parent->getDaily();
    }

    public function getMinutely(): Minutely {
        return $this->parent->getMinutely();
    }
}