<?php

namespace Flibidi67\OpenMeteo\Object\DWD;

use Flibidi67\OpenMeteo\Object\AbstractDaily;
use Flibidi67\OpenMeteo\Traits\ApparentTemperature\ApparentTemperatureMax;
use Flibidi67\OpenMeteo\Traits\ApparentTemperature\ApparentTemperatureMin;
use Flibidi67\OpenMeteo\Traits\Evapotranspiration\Et0FaoEvapotranspiration;
use Flibidi67\OpenMeteo\Traits\Precipitation\PrecipitationHours;
use Flibidi67\OpenMeteo\Traits\Precipitation\PrecipitationProbabilityMax;
use Flibidi67\OpenMeteo\Traits\Precipitation\PrecipitationSum;
use Flibidi67\OpenMeteo\Traits\Rain\RainSum;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiationSum;
use Flibidi67\OpenMeteo\Traits\Showers\ShowersSum;
use Flibidi67\OpenMeteo\Traits\Snow\SnowfallSum;
use Flibidi67\OpenMeteo\Traits\Sun\Sunrise;
use Flibidi67\OpenMeteo\Traits\Sun\Sunset;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature2mMax;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature2mMin;
use Flibidi67\OpenMeteo\Traits\UV\UVIndexClearSkyMax;
use Flibidi67\OpenMeteo\Traits\UV\UVIndexMax;
use Flibidi67\OpenMeteo\Traits\WeatherCode;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection10mDominant;
use Flibidi67\OpenMeteo\Traits\Wind\Gusts\WindGusts10mMax;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed10mMax;

class Daily extends AbstractDaily {
    use WeatherCode, Temperature2mMax, Temperature2mMin, ApparentTemperatureMax, ApparentTemperatureMin, Sunrise, Sunset,
        PrecipitationSum, RainSum, ShowersSum, SnowfallSum, PrecipitationHours, WindSpeed10mMax, WindGusts10mMax, WindDirection10mDominant,
        ShortWaveRadiationSum, Et0FaoEvapotranspiration;

    protected array $availablesParameters = [
        self::WEATHERCODE,
        self::TEMPERATURE_2M_MAX,
        self::TEMPERATURE_2M_MIN,
        self::APPARENT_TEMPERATURE_MAX,
        self::APPARENT_TEMPERATURE_MIN,
        self::SUNRISE,
        self::SUNSET,
        self::PRECIPITATION_SUM,
        self::RAIN_SUM,
        self::SHOWERS_SUM,
        self::SNOWFALL_SUM,
        self::PRECIPITATION_HOURS,
        self::WIND_SPEED_10M_MAX,
        self::WIND_GUSTS_10M_MAX,
        self::WIND_DIRECTION_10M_DOMINANT,
        self::SHORT_WAVE_RADIATION_SUM,
        self::ET0_FAO_EVAPOTRANSPIRATION,
    ];

    public function getHourly(): Hourly {
        return $this->parent->getHourly();
    }

    public function getDaily(): ?Daily {
        return $this->parent->getDaily();
    }

    public function getMinutely(): Minutely15 {
        return $this->parent->getMinutely();
    }
}