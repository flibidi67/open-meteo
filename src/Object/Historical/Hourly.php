<?php

namespace Flibidi67\OpenMeteo\Object\Historical;

use Flibidi67\OpenMeteo\Object\AbstractHourly;
use Flibidi67\OpenMeteo\Traits\ApparentTemperature\ApparentTemperature;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCover;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverHigh;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverLow;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverMid;
use Flibidi67\OpenMeteo\Traits\DewPoint2m;
use Flibidi67\OpenMeteo\Traits\Evapotranspiration\Et0FaoEvapotranspiration;
use Flibidi67\OpenMeteo\Traits\Precipitation\Precipitation;
use Flibidi67\OpenMeteo\Traits\Pressure\PressureMSL;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradiance;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiation;
use Flibidi67\OpenMeteo\Traits\Rain\Rain;
use Flibidi67\OpenMeteo\Traits\RelativeHumidity2m;
use Flibidi67\OpenMeteo\Traits\Snow\Snowfall;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture0to7cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture100to255cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture28to100cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture7to28cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature0to7cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature100to255cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature28to100cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature7to28cm;
use Flibidi67\OpenMeteo\Traits\Pressure\SurfacePressure;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature2m;
use Flibidi67\OpenMeteo\Traits\Pressure\VaporPressureDeficit;
use Flibidi67\OpenMeteo\Traits\WeatherCode;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection100m;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection10m;
use Flibidi67\OpenMeteo\Traits\Wind\Gusts\WindGusts10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed100m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed10m;

class Hourly extends AbstractHourly {
    use Temperature2m, RelativeHumidity2m, DewPoint2m, ApparentTemperature, PressureMSL, SurfacePressure, Precipitation,
        Rain, Snowfall, WeatherCode, CloudCover, CloudCoverLow, CloudCoverMid, CloudCoverHigh, ShortWaveRadiation,
        DirectRadiation, DiffuseRadiation, DirectNormalIrradiance, WindSpeed10m, WindSpeed100m, WindDirection10m, WindDirection100m,
        WindGusts10m, Et0FaoEvapotranspiration, VaporPressureDeficit, SoilTemperature0to7cm, SoilTemperature7to28cm,
        SoilTemperature28to100cm, SoilTemperature100to255cm, SoilMoisture0to7cm, SoilMoisture7to28cm, SoilMoisture28to100cm,
        SoilMoisture100to255cm;

    protected array $availablesParameters = [
        self::TEMPERATURE_2M,
        self::RELATIVE_HUMIDITY_2M,
        self::DEW_POINT_2M,
        self::APPARENT_TEMPERATURE,
        self::PRESSURE_MSL,
        self::SURFACE_PRESSURE,
        self::PRECIPITATION,
        self::RAIN,
        self::SNOWFALL,
        self::WEATHERCODE,
        self::CLOUD_COVER,
        self::CLOUD_COVER_LOW,
        self::CLOUD_COVER_MID,
        self::CLOUD_COVER_HIGH,
        self::SHORT_WAVE_RADIATION,
        self::DIRECT_RADIATION,
        self::DIFFUSE_RADIATION,
        self::DIRECT_NORMAL_IRRADIANCE,
        self::WIND_SPEED_10M,
        self::WIND_SPEED_100M,
        self::WIND_DIRECTION_10M,
        self::WIND_DIRECTION_100M,
        self::WIND_GUSTS_10M,
        self::ET0_FAO_EVAPOTRANSPIRATION,
        self::VAPOR_PRESSURE_DEFICIT,
        self::SOIL_TEMPERATURE_0_7CM,
        self::SOIL_TEMPERATURE_7_28CM,
        self::SOIL_TEMPERATURE_28_100CM,
        self::SOIL_TEMPERATURE_100_255CM,
        self::SOIL_MOISTURE_0_7CM,
        self::SOIL_MOISTURE_7_28CM,
        self::SOIL_MOISTURE_28_100CM,
        self::SOIL_MOISTURE_100_255CM
    ];

    public function getHourly(): Hourly {
        return $this->parent->getHourly();
    }

    public function getDaily(): ?Daily {
        return $this->parent->getDaily();
    }
}