<?php

namespace Flibidi67\OpenMeteo\Object;

class AbstractDaily extends AbstractObject
{
    /**
     * Return the Daily object as a querystring.
     * @return string
     */
    public function toQueryString(): string {
        return !empty($this->parameters) ? ("&daily=" . implode(",", array_unique($this->parameters))) : "";
    }

    /**
     * With all daily parameters.
     */
    public function withAll(): self {
        $this->parameters = array_values($this->availablesParameters);
        return $this;
    }

    /**
     * Add $parameter to $this->parameters.
     * @param string|array $parameter
     * @return self
     */
    public function with(string|array $parameter): self {
        if (is_string($parameter) && strpos($parameter, ",")) {
            $parameter = explode(',', $parameter);
        }
        if (is_array($parameter)) {
            $this->parameters = array_merge($this->parameters, $parameter);
        } else {
            $this->parameters[] = $parameter;
        }
        return $this;
    }
}