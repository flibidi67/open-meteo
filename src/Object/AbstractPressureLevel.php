<?php

namespace Flibidi67\OpenMeteo\Object;

use Exception;

class AbstractPressureLevel {
    
    const TYPE_TEMPERATURE = "temperature";
    const TYPE_DEWPOINT = "dewpoint";
    const TYPE_RELATIVE_HUMIDITY = "relativehumidity";
    const TYPE_SPECIFIC_HUMIDITY = "specific_humidity";
    const TYPE_CLOUD_COVER = "cloudcover";
    const TYPE_WIND_SPEED = "windspeed";
    const TYPE_WIND_DIRECTION = "winddirection";
    const TYPE_GEOPOTENTIAL_HEIGHT = "geopotential_height";
    const TYPE_DIVERGENCE_OF_WIND = "divergence_of_wind";
    const TYPE_ATMOSPHERE_RELATIVE_VORTICITY = "atmosphere_relative_vorticity";

    // To override in extended class.
    const TYPES = [];
    // To override in extended class.
    const AVAILABLES_PRESSURES = [];

    private string $type;
    private array $parameters = [];

    public function __construct(string $type) {
        if (!in_array($type, $this->getTypes())) {
            throw new Exception("The type $type doesn't exists for the pressure.");
        }
        $this->type = $type;
    }

    /**
     * Return the PressureLevel object as a querystring.
     * @return string
     */
    public function toQueryString(): string {
        if (!empty($this->parameters)) {
            return implode(",", array_unique($this->parameters));
        }
        return "";
    }

    /**
     * Add $pressure into $this->parameters.
     * @param int $pressure
     * @return self
     */
    public function withPressure(int $pressure): self {
        if (in_array($pressure, $this->getAvailablesPressures())) {
            $this->parameters[] = $this->type . '_' . $pressure . 'hPa';
        }
        return $this;
    }

    /**
     * Return the available types. Must be overridden in herited classes.
     * @return array
     */
    protected function getTypes(): array {
        return self::TYPES;
    }

    /**
     * Return the available pressures. Must be overridden in herited classes.
     * @return array
     */
    protected function getAvailablesPressures(): array {
        return self::AVAILABLES_PRESSURES;
    }
}