<?php

namespace Flibidi67\OpenMeteo\Object\METNo;

use Flibidi67\OpenMeteo\Object\AbstractDaily;
use Flibidi67\OpenMeteo\Object\AbstractHourly;
use Flibidi67\OpenMeteo\Traits\ApparentTemperature\ApparentTemperature;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCover;
use Flibidi67\OpenMeteo\Traits\DewPoint2m;
use Flibidi67\OpenMeteo\Traits\Evapotranspiration\Et0FaoEvapotranspiration;
use Flibidi67\OpenMeteo\Traits\Precipitation\Precipitation;
use Flibidi67\OpenMeteo\Traits\Pressure\PressureMSL;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradiance;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradianceInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiationInstant;
use Flibidi67\OpenMeteo\Traits\RelativeHumidity2m;
use Flibidi67\OpenMeteo\Traits\Snow\Snowfall;
use Flibidi67\OpenMeteo\Traits\Pressure\SurfacePressure;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature2m;
use Flibidi67\OpenMeteo\Traits\Pressure\VaporPressureDeficit;
use Flibidi67\OpenMeteo\Traits\WeatherCode;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection10m;
use Flibidi67\OpenMeteo\Traits\Wind\Gusts\WindGusts10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed10m;

class Hourly extends AbstractHourly {
    use Temperature2m, ApparentTemperature, DewPoint2m, RelativeHumidity2m, WeatherCode, CloudCover, PressureMSL, SurfacePressure,
        Precipitation, Snowfall, Et0FaoEvapotranspiration, VaporPressureDeficit, WindSpeed10m, WindDirection10m, WindGusts10m,
        ShortWaveRadiation, DirectRadiation, DiffuseRadiation, DirectNormalIrradiance, TerrestrialRadiation, ShortWaveRadiationInstant,
        DirectRadiationInstant, DiffuseRadiationInstant, DirectNormalIrradianceInstant, TerrestrialRadiationInstant;

    protected array $availablesParameters = [
        self::TEMPERATURE_2M,
        self::APPARENT_TEMPERATURE,
        self::DEW_POINT_2M,
        self::RELATIVE_HUMIDITY_2M,
        self::WEATHERCODE,
        self::CLOUD_COVER,
        self::PRESSURE_MSL,
        self::SURFACE_PRESSURE,
        self::PRECIPITATION,
        self::SNOWFALL,
        self::ET0_FAO_EVAPOTRANSPIRATION,
        self::VAPOR_PRESSURE_DEFICIT,
        self::WIND_SPEED_10M,
        self::WIND_DIRECTION_10M,
        self::WIND_GUSTS_10M,
        self::SHORT_WAVE_RADIATION,
        self::DIRECT_RADIATION,
        self::DIFFUSE_RADIATION,
        self::DIRECT_NORMAL_IRRADIANCE,
        self::TERRESTRIAL_RADIATION,
        self::SHORT_WAVE_RADIATION_INSTANT,
        self::DIRECT_RADIATION_INSTANT,
        self::DIFFUSE_RADIATION_INSTANT,
        self::DIRECT_NORMAL_IRRADIANCE_INSTANT,
        self::TERRESTRIAL_RADIATION_INSTANT
    ];

    public function getHourly(): Hourly {
        return $this->parent->getHourly();
    }

    public function getDaily(): ?AbstractDaily {
        return $this->parent->getDaily();
    }
}