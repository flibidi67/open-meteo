<?php

namespace Flibidi67\OpenMeteo\Object;

use Datetime;
use Exception;

class Settings extends AbstractObject {
    
    const DEFAULT_TEMPERATURE_UNIT = self::TEMPERATURE_CELCIUS;
    const DEFAULT_WIND_SPEED_UNIT = self::WIND_SPEED_KMH;
    const DEFAULT_PRECIPITATION_UNIT = self::PRECIPITATION_MM;
    const DEFAULT_TIMEFORMAT = self::TIMEFORMAT_ISO;
    const DEFAULT_FORECASTS_DAYS = 7;
    const DEFAULT_PAST_DAYS = 0;
    const DEFAULT_TIMEZONE = "GMT";
    const DEFAULT_CURRENT_WEATHER = false;

    const TEMPERATURE_UNIT = "temperature_unit";
    const WIND_SPEED_UNIT = "windspeed_unit";
    const PRECIPITATION_UNIT = "precipitation_unit";
    const TIMEFORMAT = "timeformat";
    const FORECAST_DAYS = "forecast_days";
    const PAST_DAYS = "past_days";
    const START_DATE = "start_date";
    const END_DATE = "end_date";
    const TIMEZONE = "timezone";
    const CURRENT_WEATHER = "current_weather";

    const TEMPERATURE_CELCIUS = "celcius";
    const TEMPERATURE_FAHRENHEIT = "fahrenheit";
    const TEMPERATURE_UNITS = [
        self::TEMPERATURE_CELCIUS,
        self::TEMPERATURE_FAHRENHEIT
    ];

    const WIND_SPEED_KMH = "kmh";
    const WIND_SPEED_MS = "ms";
    const WIND_SPEED_MPH = "mph";
    const WIND_SPEED_KN = "kn";
    const WIND_SPEED_UNITS = [
        self::WIND_SPEED_KMH,
        self::WIND_SPEED_MS,
        self::WIND_SPEED_MPH,
        self::WIND_SPEED_KN
    ];

    const PRECIPITATION_MM = "mm";
    const PRECIPITATION_INCH = "inch";
    const PRECIPITATION_UNITS = [
        self::PRECIPITATION_MM,
        self::PRECIPITATION_INCH
    ];

    const TIMEFORMAT_ISO = "iso8601";
    const TIMEFORMAT_UNIX = "unixtime";
    const TIMEFORMATS = [
        self::TIMEFORMAT_ISO,
        self::TIMEFORMAT_UNIX
    ];

    const MIN_FORECAST_DAYS = 0;
    const MAX_FORECAST_DAYS = 16;

    const MIN_PAST_DAYS = 0;
    const MAX_PAST_DAYS = 92;

    protected array $availablesParameters = [
        self::TEMPERATURE_UNIT,
        self::WIND_SPEED_UNIT,
        self::PRECIPITATION_UNIT,
        self::TIMEFORMAT,
        self::FORECAST_DAYS,
        self::PAST_DAYS,
        self::START_DATE,
        self::END_DATE,
        self::TIMEZONE,
        self::CURRENT_WEATHER,
    ];

    private ?string $temperatureUnit = null;
    private ?string $windSpeedUnit = null;
    private ?string $precipitationUnit = null;
    private ?string $timeformat = null;
    private ?int $pastDays = null;
    private ?int $forecastDays = null;
    private ?Datetime $startDate = null;
    private ?Datetime $endDate = null;
    private ?string $timezone = null;
    private ?bool $currentWeather = null;

    /**
     * Return the timezone.
     * @return string|null
     */
    public function getTimezone(): ?string {
        return $this->timezone;
    }

    /**
     * Return the start date
     * @return Datetime|null
     */
    public function getStartDate(): ?Datetime {
        return $this->startDate;
    }

    /**
     * Return the end date
     * @return Datetime|null
     */
    public function getEndDate(): ?Datetime {
        return $this->endDate;
    }

    /**
     * Return the Settings object as a querystring.
     * In order to limit the query size, if the settings has the same value as the default, it'll not be added to the
     * querystring.
     * @return string
     */
    public function toQueryString(): string {
        $parameters = [];
        if (!is_null($this->temperatureUnit) && $this->temperatureUnit !== self::DEFAULT_TEMPERATURE_UNIT) {
            $parameters[] = self::TEMPERATURE_UNIT . "=" . $this->temperatureUnit;
        }
        if (!is_null($this->windSpeedUnit) && $this->windSpeedUnit !== self::DEFAULT_WIND_SPEED_UNIT) {
            $parameters[] = self::WIND_SPEED_UNIT . "=" . $this->windSpeedUnit;
        }
        if (!is_null($this->precipitationUnit) && $this->precipitationUnit !== self::DEFAULT_PRECIPITATION_UNIT) {
            $parameters[] = self::PRECIPITATION_UNIT . "=" . $this->precipitationUnit;
        }
        if (!is_null($this->timeformat) && $this->timeformat !== self::DEFAULT_TIMEFORMAT) {
            $parameters[] = self::TIMEFORMAT . "=" . $this->timeformat;
        }
        if (!is_null($this->forecastDays) && $this->forecastDays !== self::DEFAULT_FORECASTS_DAYS) {
            $parameters[] = self::FORECAST_DAYS . "=" . $this->forecastDays;
        }
        if (!is_null($this->startDate) && !is_null($this->endDate)) {
            $parameters[] = self::START_DATE . "=" . $this->startDate->format("Y-m-d");
            $parameters[] = self::END_DATE . "=" . $this->endDate->format("Y-m-d");
        }
        if (!is_null($this->timezone)) {
            $parameters[] = self::TIMEZONE . "=" . $this->timezone;
        }
        if (!is_null($this->currentWeather) && $this->currentWeather !== self::DEFAULT_CURRENT_WEATHER) {
            $parameters[] = self::CURRENT_WEATHER . "=" . $this->currentWeather;
        }
        return empty($parameters) ? "" : "&" . implode("&", $parameters);
    }

    /**
     * With an array, as the config file, filed all the settings.
     * @param array $settings
     * @return Settings
     */
    public function setAll(array $settings): self {
        foreach ($settings as $setting => $value) {
            switch ($setting) {
                case 'temperature_unit':
                    $this->temperatureUnit = (string) $value;
                    break;
                case 'wind_speed_unit':
                    $this->windSpeedUnit = (string) $value;
                    break;
                case 'precipitation_unit':
                    $this->precipitationUnit = (string) $value;
                    break;
                case 'timeformat':
                    $this->timeformat = (string) $value;
                    break;
                case 'timezone':
                    $this->timezone = (string) $value;
                    break;
                case 'past_days':
                    $this->pastDays = (int) $value;
                    break;
                case 'forecast_days':
                    $this->forecastDays = (int) $value;
                    break;
                case 'current_weather':
                    $this->currentWeather = (bool) $value;
                    break;
            }
        }
        return $this;
    }

    /**
     * Set the temperature unit.
     * @param string $temperatureUnit
     * @return self
     * @throws Exception
     */
    public function setTemperatureUnit(string $temperatureUnit): self {
        if (in_array($temperatureUnit, self::TEMPERATURE_UNITS)) {
            $this->temperatureUnit = $temperatureUnit;
            return $this;
        }
        throw new Exception("Temperature unit $temperatureUnit doesn't exists choose " . implode(", ", self::TEMPERATURE_UNITS) . ".");
    }

    /**
     * Set the precipitation unit.
     * @param int $precipitationUnit
     * @return self
     * @throws Exception
     */
    public function setPrecipitationUnit(int $precipitationUnit): self {
        if (in_array($precipitationUnit, self::PRECIPITATION_UNITS)) {
            $this->precipitationUnit = $precipitationUnit;
            return $this;
        }
        throw new Exception("Precipitation unit $precipitationUnit doesn't exists choose " . implode(", ", self::PRECIPITATION_UNITS) . ".");
    }

    /**
     * Set the wind speed unit.
     * @param string $windSpeedUnit
     * @return self
     * @throws Exception
     */
    public function setWindSpeedUnit(string $windSpeedUnit): self {
        if (in_array($windSpeedUnit, self::WIND_SPEED_UNITS)) {
            $this->windSpeedUnit = $windSpeedUnit;
            return $this;
        }
        throw new Exception("Wind speed unit $windSpeedUnit doesn't exists choose " . implode(", ", self::WIND_SPEED_UNITS) . ".");
    }

    /**
     * Set the timeformat.
     * @param string $windSpeedUnit
     * @return self
     * @throws Exception
     */
    public function setTimeformat(string $timeformat): self {
        if (in_array($timeformat, self::TIMEFORMATS)) {
            $this->timeformat = $timeformat;
            return $this;
        }
        throw new Exception("Timeformat $timeformat doesn't exists choose " . implode(", ", self::TIMEFORMATS) . ".");
    }

    /**
     * Set the past days.
     * @param int $pastDays
     * @return self
     * @throws Exception
     */
    public function setPastDays(int $pastDays): self {
        if ($pastDays >= self::MIN_PAST_DAYS && $pastDays <= self::MAX_PAST_DAYS) {
            if ($pastDays > self::MIN_PAST_DAYS && ($this->startDate || $this->endDate)) {
                throw new Exception("You cannot set the settings past days with start date and/or end date.");
            }
            $this->pastDays = $pastDays;
            return $this;
        }
        throw new Exception("Past days must be in the range " . self::MIN_PAST_DAYS . " to " . self::MAX_PAST_DAYS . ", $pastDays given.");
    }

    /**
     * Set the forecast days.
     * @param int $pastDays
     * @return self
     * @throws Exception
     */
    public function setForecastDays(int $forecastDays): self {
        if ($forecastDays >= self::MIN_FORECAST_DAYS && $forecastDays <= self::MAX_FORECAST_DAYS) {
            $this->forecastDays = $forecastDays;
            return $this;
        }
        throw new Exception("Forecast days must be in the range " . self::MIN_FORECAST_DAYS . " to " . self::MAX_FORECAST_DAYS . ", $forecastDays given.");
    }

    /**
     * Set the start date.
     * @param Datetime
     * @return self
     * @throws Exception
     */
    public function setStartDate(Datetime $startDate): self {
        if (!is_null($this->pastDays) && $this->pastDays > 0) {
            throw new Exception("You cannot set the settings past days with start date and/or end date.");
        }
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * Set the start date.
     * @param Datetime
     * @return self
     * @throws Exception
     */
    public function setEndDate(Datetime $endDate): self {
        if (!is_null($this->pastDays) && $this->pastDays > 0) {
            throw new Exception("You cannot set the settings past days with start date and/or end date.");
        }
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * Set the timezone
     * @param string $timezone
     * @return self
     * @throws Exception
     */
    public function setTimezone(string $timezone): self {
        if ($timezone !== "auto" && $timezone !== "GMT" && !in_array($timezone, timezone_identifiers_list())) {
            throw new Exception("$timezone must be valid, see timezone_identifiers_list(), auto and GMT are also valid.");
        }
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * Set the current weather param.
     * @param bool $currentWeather
     * @return self
     */
    public function setCurrentWeather(bool $currentWeather): self {
        $this->currentWeather = $currentWeather;
        return $this;
    }
}