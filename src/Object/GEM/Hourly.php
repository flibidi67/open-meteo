<?php

namespace Flibidi67\OpenMeteo\Object\GEM;

use Flibidi67\OpenMeteo\Object\AbstractHourly;
use Flibidi67\OpenMeteo\Service\AbstractOpenMeteoService;
use Flibidi67\OpenMeteo\Traits\ApparentTemperature\ApparentTemperature;
use Flibidi67\OpenMeteo\Traits\CAPE;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCover;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverHigh;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverLow;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverMid;
use Flibidi67\OpenMeteo\Traits\DewPoint2m;
use Flibidi67\OpenMeteo\Traits\Evapotranspiration\Et0FaoEvapotranspiration;
use Flibidi67\OpenMeteo\Traits\Precipitation\Precipitation;
use Flibidi67\OpenMeteo\Traits\Pressure\PressureMSL;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradiance;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradianceInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Rain\Rain;
use Flibidi67\OpenMeteo\Traits\RelativeHumidity2m;
use Flibidi67\OpenMeteo\Traits\Showers\Showers;
use Flibidi67\OpenMeteo\Traits\Snow\Snowfall;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture0to10cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature0to10cm;
use Flibidi67\OpenMeteo\Traits\Pressure\SurfacePressure;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature120m;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature2m;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature40m;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature80m;
use Flibidi67\OpenMeteo\Traits\Pressure\VaporPressureDeficit;
use Flibidi67\OpenMeteo\Traits\WeatherCode;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection10m;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection120m;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection40m;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection80m;
use Flibidi67\OpenMeteo\Traits\Wind\Gusts\WindGusts10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed120m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed40m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed80m;

class Hourly extends AbstractHourly {
    use Temperature2m, RelativeHumidity2m, DewPoint2m, ApparentTemperature, Precipitation, Rain, Showers, Snowfall,
        WeatherCode, PressureMSL, SurfacePressure, CloudCover, CloudCoverLow, CloudCoverMid, CloudCoverHigh, Et0FaoEvapotranspiration,
        VaporPressureDeficit, CAPE, WindSpeed10m, WindSpeed40m, WindSpeed80m, WindSpeed120m, WindDirection10m, WindDirection40m,
        WindDirection80m, WindDirection120m, WindGusts10m, Temperature40m, Temperature80m, Temperature120m, SoilTemperature0to10cm,
        SoilMoisture0to10cm, ShortWaveRadiation, DirectRadiation, DiffuseRadiation, DirectNormalIrradiance, TerrestrialRadiation,
        ShortWaveRadiationInstant, DirectRadiationInstant, DiffuseRadiationInstant, DirectNormalIrradianceInstant, TerrestrialRadiationInstant;

    protected array $availablesParameters = [
        self::TEMPERATURE_2M,
        self::RELATIVE_HUMIDITY_2M,
        self::DEW_POINT_2M,
        self::APPARENT_TEMPERATURE,
        self::PRECIPITATION,
        self::RAIN,
        self::SHOWERS,
        self::SNOWFALL,
        self::WEATHERCODE,
        self::PRESSURE_MSL,
        self::SURFACE_PRESSURE,
        self::CLOUD_COVER,
        self::CLOUD_COVER_LOW,
        self::CLOUD_COVER_MID,
        self::CLOUD_COVER_HIGH,
        self::ET0_FAO_EVAPOTRANSPIRATION,
        self::VAPOR_PRESSURE_DEFICIT,
        self::CAPE,
        self::WIND_SPEED_10M,
        self::WIND_SPEED_40M,
        self::WIND_SPEED_80M,
        self::WIND_SPEED_120M,
        self::WIND_DIRECTION_10M,
        self::WIND_DIRECTION_40M,
        self::WIND_DIRECTION_80M,
        self::WIND_DIRECTION_120M,
        self::WIND_GUSTS_10M,
        self::TEMPERATURE_40M,
        self::TEMPERATURE_80M,
        self::TEMPERATURE_120M,
        self::SOIL_TEMPERATURE_0_10CM,
        self::SOIL_MOISTURE_0_10CM,
        self::SHORT_WAVE_RADIATION,
        self::DIRECT_RADIATION,
        self::DIFFUSE_RADIATION,
        self::DIRECT_NORMAL_IRRADIANCE,
        self::TERRESTRIAL_RADIATION,
        self::SHORT_WAVE_RADIATION_INSTANT,
        self::DIRECT_RADIATION_INSTANT,
        self::DIFFUSE_RADIATION_INSTANT,
        self::DIRECT_NORMAL_IRRADIANCE_INSTANT,
        self::TERRESTRIAL_RADIATION_INSTANT
    ];

    public function __construct(AbstractOpenMeteoService $parent) {
        $this->pressureLevelClass = PressureLevel::class;
        parent::__construct($parent);
    }

    public function getHourly(): Hourly {
        return $this->parent->getHourly();
    }

    public function getDaily(): ?Daily {
        return $this->parent->getDaily();
    }
}