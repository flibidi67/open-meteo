<?php

namespace Flibidi67\OpenMeteo\Object\GFS;

use Flibidi67\OpenMeteo\Object\AbstractPressureLevel;

class PressureLevel extends AbstractPressureLevel {

    const TYPES = [
        self::TYPE_TEMPERATURE,
        self::TYPE_DEWPOINT,
        self::TYPE_RELATIVE_HUMIDITY,
        self::TYPE_CLOUD_COVER,
        self::TYPE_WIND_SPEED,
        self::TYPE_WIND_DIRECTION,
        self::TYPE_GEOPOTENTIAL_HEIGHT,
    ];

    const AVAILABLES_PRESSURES = [
        10,
        15,
        20,
        30,
        40,
        50,
        70,
        100,
        150,
        200,
        250,
        300,
        350,
        400,
        450,
        500,
        550,
        600,
        650,
        700,
        750,
        800,
        850,
        900,
        925,
        950,
        975,
        1000
    ];

    private string $type;
    private array $parameters = [];

    protected function getTypes(): array {
        return self::TYPES;
    }

    protected function getAvailablesPressures(): array {
        return self::AVAILABLES_PRESSURES;
    }
}