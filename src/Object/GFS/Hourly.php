<?php

namespace Flibidi67\OpenMeteo\Object\GFS;

use Flibidi67\OpenMeteo\Object\AbstractHourly;
use Flibidi67\OpenMeteo\Service\AbstractOpenMeteoService;
use Flibidi67\OpenMeteo\Traits\ApparentTemperature\ApparentTemperature;
use Flibidi67\OpenMeteo\Traits\CAPE;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCover;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverHigh;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverLow;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverMid;
use Flibidi67\OpenMeteo\Traits\DewPoint2m;
use Flibidi67\OpenMeteo\Traits\Evapotranspiration\Et0FaoEvapotranspiration;
use Flibidi67\OpenMeteo\Traits\Evapotranspiration\Evapotranspiration;
use Flibidi67\OpenMeteo\Traits\LiftedIndex;
use Flibidi67\OpenMeteo\Traits\Precipitation\PrecipitationProbability;
use Flibidi67\OpenMeteo\Traits\Pressure\PressureMSL;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradiance;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradianceInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiationInstant;
use Flibidi67\OpenMeteo\Traits\RelativeHumidity2m;
use Flibidi67\OpenMeteo\Traits\Snow\Snowfall;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture0to10cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture100to200cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture10to40cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture40to100cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature0to10cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature100to200cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature10to40cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature40to100cm;
use Flibidi67\OpenMeteo\Traits\Pressure\SurfacePressure;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature2m;
use Flibidi67\OpenMeteo\Traits\Pressure\VaporPressureDeficit;
use Flibidi67\OpenMeteo\Traits\WeatherCode;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection10m;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection80m;
use Flibidi67\OpenMeteo\Traits\Wind\Gusts\WindGusts10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed80m;

class Hourly extends AbstractHourly {
    use Temperature2m, RelativeHumidity2m, DewPoint2m, ApparentTemperature, PressureMSL, SurfacePressure, Snowfall, PrecipitationProbability,
        WeatherCode, CloudCover, CloudCoverLow, CloudCoverMid, CloudCoverHigh, Evapotranspiration, Et0FaoEvapotranspiration,
        VaporPressureDeficit, CAPE, LiftedIndex, WindSpeed10m, WindSpeed80m, WindDirection10m, WindDirection80m, WindGusts10m,
        SoilTemperature0to10cm, SoilTemperature10to40cm, SoilTemperature40to100cm, SoilTemperature100to200cm, SoilMoisture0to10cm,
        SoilMoisture10to40cm, SoilMoisture40to100cm, SoilMoisture100to200cm, ShortWaveRadiation, DirectRadiation, DiffuseRadiation,
        DirectNormalIrradiance, TerrestrialRadiation, ShortWaveRadiationInstant, DirectRadiationInstant, DiffuseRadiationInstant,
        DirectNormalIrradianceInstant, TerrestrialRadiationInstant;

    protected array $availablesParameters = [
        self::TEMPERATURE_2M,
        self::RELATIVE_HUMIDITY_2M,
        self::DEW_POINT_2M,
        self::APPARENT_TEMPERATURE,
        self::PRESSURE_MSL,
        self::SURFACE_PRESSURE,
        self::SNOWFALL,
        self::PRECIPITATION_PROBABILITY,
        self::WEATHERCODE,
        self::CLOUD_COVER,
        self::CLOUD_COVER_LOW,
        self::CLOUD_COVER_MID,
        self::CLOUD_COVER_HIGH,
        self::EVAPOTRANSPIRATION,
        self::ET0_FAO_EVAPOTRANSPIRATION,
        self::VAPOR_PRESSURE_DEFICIT,
        self::CAPE,
        self::LIFTED_INDEX,
        self::WIND_SPEED_10M,
        self::WIND_SPEED_80M,
        self::WIND_DIRECTION_10M,
        self::WIND_DIRECTION_80M,
        self::WIND_GUSTS_10M,
        self::SOIL_TEMPERATURE_0_10CM,
        self::SOIL_TEMPERATURE_10_40CM,
        self::SOIL_TEMPERATURE_40_100CM,
        self::SOIL_TEMPERATURE_100_200CM,
        self::SOIL_MOISTURE_0_10CM,
        self::SOIL_MOISTURE_10_40CM,
        self::SOIL_MOISTURE_40_100CM,
        self::SOIL_MOISTURE_100_200CM,
        self::SHORT_WAVE_RADIATION,
        self::DIFFUSE_RADIATION,
        self::DIRECT_RADIATION,
        self::DIRECT_NORMAL_IRRADIANCE,
        self::TERRESTRIAL_RADIATION,
        self::SHORT_WAVE_RADIATION_INSTANT,
        self::DIRECT_RADIATION_INSTANT,
        self::DIFFUSE_RADIATION_INSTANT,
        self::DIRECT_NORMAL_IRRADIANCE_INSTANT,
        self::TERRESTRIAL_RADIATION_INSTANT
    ];

    public function __construct(AbstractOpenMeteoService $parent) {
        $this->pressureLevelClass = PressureLevel::class;
        parent::__construct($parent);
    }

    public function getHourly(): Hourly {
        return $this->parent->getHourly();
    }

    public function getDaily(): ?Daily {
        return $this->parent->getDaily();
    }
}