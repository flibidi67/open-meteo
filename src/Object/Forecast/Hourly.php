<?php

namespace Flibidi67\OpenMeteo\Object\Forecast;

use Flibidi67\OpenMeteo\Object\AbstractHourly;
use Flibidi67\OpenMeteo\Service\AbstractOpenMeteoService;
use Flibidi67\OpenMeteo\Traits\ApparentTemperature\ApparentTemperature;
use Flibidi67\OpenMeteo\Traits\CAPE;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCover;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverHigh;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverLow;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverMid;
use Flibidi67\OpenMeteo\Traits\DewPoint2m;
use Flibidi67\OpenMeteo\Traits\Evapotranspiration\Et0FaoEvapotranspiration;
use Flibidi67\OpenMeteo\Traits\Evapotranspiration\Evapotranspiration;
use Flibidi67\OpenMeteo\Traits\FreezingLevelHeight;
use Flibidi67\OpenMeteo\Traits\Precipitation\Precipitation;
use Flibidi67\OpenMeteo\Traits\Precipitation\PrecipitationProbability;
use Flibidi67\OpenMeteo\Traits\Pressure\PressureMSL;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DiffuseRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradiance;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectNormalIrradianceInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\DirectRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\ShortWaveRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiation;
use Flibidi67\OpenMeteo\Traits\Radiation\TerrestrialRadiationInstant;
use Flibidi67\OpenMeteo\Traits\Rain\Rain;
use Flibidi67\OpenMeteo\Traits\RelativeHumidity2m;
use Flibidi67\OpenMeteo\Traits\Showers\Showers;
use Flibidi67\OpenMeteo\Traits\Snow\SnowDepth;
use Flibidi67\OpenMeteo\Traits\Snow\Snowfall;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture0to1cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture1to3cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture27to81cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture3to9cm;
use Flibidi67\OpenMeteo\Traits\Soil\Moisture\SoilMoisture9to27cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature0cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature18cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature54cm;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature6cm;
use Flibidi67\OpenMeteo\Traits\Pressure\SurfacePressure;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature120m;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature180m;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature2m;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature80m;
use Flibidi67\OpenMeteo\Traits\UV\UVIndex;
use Flibidi67\OpenMeteo\Traits\UV\UVIndexClearSky;
use Flibidi67\OpenMeteo\Traits\Pressure\VaporPressureDeficit;
use Flibidi67\OpenMeteo\Traits\Visibility;
use Flibidi67\OpenMeteo\Traits\WeatherCode;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection10m;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection120m;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection180m;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection80m;
use Flibidi67\OpenMeteo\Traits\Wind\Gusts\WindGusts10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed120m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed180m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed80m;

class Hourly extends AbstractHourly {
    use WeatherCode, Temperature2m, Temperature80m, Temperature120m, Temperature180m, RelativeHumidity2m, DewPoint2m, 
        ApparentTemperature, Precipitation, PrecipitationProbability, Rain, Showers, Snowfall, SnowDepth, PressureMSL, SurfacePressure,
        CloudCover, CloudCoverLow, CloudCoverMid, CloudCoverHigh, Visibility, Evapotranspiration, Et0FaoEvapotranspiration, VaporPressureDeficit,
        WindSpeed10m, WindSpeed80m, WindSpeed120m, WindSpeed180m, WindDirection10m, WindDirection80m, WindDirection120m, WindDirection180m,
        WindGusts10m, SoilTemperature0cm, SoilTemperature6cm, SoilTemperature18cm, SoilTemperature54cm, SoilMoisture0to1cm, SoilMoisture1to3cm,
        SoilMoisture3to9cm, SoilMoisture9to27cm, SoilMoisture27to81cm, UVIndex, UVIndexClearSky, CAPE, FreezingLevelHeight, ShortWaveRadiation,
        DiffuseRadiation, DirectRadiation, DirectNormalIrradiance, ShortWaveRadiationInstant, TerrestrialRadiation, DirectRadiationInstant,
        DiffuseRadiationInstant, DirectNormalIrradianceInstant, TerrestrialRadiationInstant;
    
    protected array $availablesParameters = [
        self::CAPE,
        self::TEMPERATURE_2M,
        self::TEMPERATURE_80M,
        self::TEMPERATURE_120M,
        self::TEMPERATURE_180M,
        self::RELATIVE_HUMIDITY_2M,
        self::DEW_POINT_2M,
        self::APPARENT_TEMPERATURE,
        self::PRECIPITATION,
        self::PRECIPITATION_PROBABILITY,
        self::RAIN,
        self::SHOWERS,
        self::SNOWFALL,
        self::SNOW_DEPTH,
        self::WEATHERCODE,
        self::PRESSURE_MSL,
        self::SURFACE_PRESSURE,
        self::CLOUD_COVER,
        self::CLOUD_COVER_LOW,
        self::CLOUD_COVER_MID,
        self::CLOUD_COVER_HIGH,
        self::VISIBILITY,
        self::EVAPOTRANSPIRATION,
        self::ET0_FAO_EVAPOTRANSPIRATION,
        self::VAPOR_PRESSURE_DEFICIT,
        self::WIND_SPEED_10M,
        self::WIND_SPEED_80M,
        self::WIND_SPEED_120M,
        self::WIND_SPEED_180M,
        self::WIND_DIRECTION_10M,
        self::WIND_DIRECTION_80M,
        self::WIND_DIRECTION_120M,
        self::WIND_DIRECTION_180M,
        self::WIND_GUSTS_10M,
        self::TEMPERATURE_80M,
        self::TEMPERATURE_120M,
        self::TEMPERATURE_180M,
        self::SOIL_TEMPERATURE_0CM,
        self::SOIL_TEMPERATURE_6CM,
        self::SOIL_TEMPERATURE_18CM,
        self::SOIL_TEMPERATURE_54CM,
        self::SOIL_MOISTURE_0_1CM,
        self::SOIL_MOISTURE_1_3CM,
        self::SOIL_MOISTURE_3_9CM,
        self::SOIL_MOISTURE_9_27CM,
        self::SOIL_MOISTURE_27_81CM,
        self::UV_INDEX, 
        self::UV_INDEX_CLEAR_SKY,
        self::SHORT_WAVE_RADIATION,
        self::DIFFUSE_RADIATION,
        self::DIRECT_RADIATION,
        self::DIRECT_NORMAL_IRRADIANCE,
        self::TERRESTRIAL_RADIATION,
        self::SHORT_WAVE_RADIATION_INSTANT,
        self::DIRECT_RADIATION_INSTANT,
        self::DIFFUSE_RADIATION_INSTANT,
        self::DIRECT_NORMAL_IRRADIANCE_INSTANT,
        self::TERRESTRIAL_RADIATION_INSTANT,
        self::FREEZING_LEVEL_HEIGHT
    ];

    public function __construct(AbstractOpenMeteoService $parent) {
        $this->pressureLevelClass = PressureLevel::class;
        parent::__construct($parent);
    }

    public function getHourly(): Hourly {
        return $this->parent->getHourly();
    }

    public function getDaily(): ?Daily {
        return $this->parent->getDaily();
    }
}