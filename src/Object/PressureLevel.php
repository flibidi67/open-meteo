<?php

namespace Flibidi67\OpenMeteo\Object;

class PressureLevel {
    const TYPE_TEMPERATURE = "temperature";
    const TYPE_DEWPOINT = "dewpoint";
    const TYPE_RELATIVE_HUMIDITY = "relativehumidity";
    const TYPE_CLOUD_COVER = "cloudcover";
    const TYPE_WIND_SPEED = "windspeed";
    const TYPE_WIND_DIRECTION = "winddirection";
    const TYPE_GEOPOTENTIAL_HEIGHT = "geopotential_height";
    const TYPES = [
        self::TYPE_TEMPERATURE,
        self::TYPE_DEWPOINT,
        self::TYPE_RELATIVE_HUMIDITY,
        self::TYPE_CLOUD_COVER,
        self::TYPE_WIND_SPEED,
        self::TYPE_WIND_DIRECTION,
        self::TYPE_GEOPOTENTIAL_HEIGHT,
    ];

    const AVAILABLES_PRESSURES = [
        30,
        50,
        70,
        100,
        150,
        200,
        250,
        300,
        400,
        500,
        600,
        700,
        800,
        850,
        900,
        925,
        950,
        975,
        1000
    ];

    private string $type;
    private array $parameters = [];
    
    public function __construct(string $type) {
        if (!in_array($type, self::TYPES)) {
            throw new Exception("The type $type doesn't exists for the pressure.");
        }
        $this->type = $type;
    }

    /**
     * Return the PressureLevel object as a querystring.
     * @return string
     */
    public function toQueryString(): string {
        if (!empty($this->parameters)) {
            return implode(",", array_unique($this->parameters));
        }
        return "";
    }

    /**
     * Add $pressure into $this->parameters.
     * @param int $pressure
     * @return self
     */
    public function withPressure(int $pressure): self {
        if (in_array($pressure, self::AVAILABLES_PRESSURES)) {
            $this->parameters[] = $this->type . '_' . $pressure . 'hPa';
        }
        return $this;
    }

    /**
     * Return all the pressure levels availables params.
     * @return array
     */
    public static function getAvailableParams(): array {
        $availablesParams = [];

        foreach (self::TYPES as $type) {
            foreach (self::AVAILABLES_PRESSURES as $pressure) {
                $availablesParams[] = $type . "_" . $pressure . "hPa";
            }
        }
        return $availablesParams;
    }
}