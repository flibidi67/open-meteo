<?php

namespace Flibidi67\OpenMeteo\Object;

use Flibidi67\OpenMeteo\Service\AbstractOpenMeteoService;

abstract class AbstractObject {

    protected array $parameters = [];
    protected array $availablesParameters = [];
    protected AbstractOpenMeteoService $parent;

    public function __construct(AbstractOpenMeteoService $parent) {
        $this->parent = $parent;
    }

    /**
     * Return the Settings object
     * @return Settings
     */
    public function getSettings(): Settings {
        return $this->parent->getSettings();
    }

    /**
     * Return the Settings object
     * @return AbstractHourly
     */
    public function getHourly(): AbstractHourly {
        return $this->parent->getHourly();
    }

    /**
     * Return the Settings object
     * @return AbstractDaily|null
     */
    public function getDaily(): ?AbstractDaily {
        return $this->parent->getDaily();
    }

    /**
     * Return all the available params.
     * @return array
     */
    public function getAvailablesParameters(): array {
        return $this->availablesParameters;
    }

    /**
     * Return the parameters.
     * @return array
     */
    public function getParameters(): array {
        return $this->parameters;
    }

    /**
     * Call the $parent get method.
     * @return array
     * @throws \Exception
     */
    public function get(): array {
        return $this->parent->get();
    }

    abstract public function toQueryString(): string;
}