<?php

namespace Flibidi67\OpenMeteo\Object\ECMWF;

use Flibidi67\OpenMeteo\Object\AbstractDaily;
use Flibidi67\OpenMeteo\Object\AbstractHourly;
use Flibidi67\OpenMeteo\Service\AbstractOpenMeteoService;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCover;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverHigh;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverLow;
use Flibidi67\OpenMeteo\Traits\CloudCover\CloudCoverMid;
use Flibidi67\OpenMeteo\Traits\Precipitation\Precipitation;
use Flibidi67\OpenMeteo\Traits\Pressure\PressureMSL;
use Flibidi67\OpenMeteo\Traits\RunOff;
use Flibidi67\OpenMeteo\Traits\SkinTemperature;
use Flibidi67\OpenMeteo\Traits\Snow\Snowfall;
use Flibidi67\OpenMeteo\Traits\Soil\Temperature\SoilTemperature0to7cm;
use Flibidi67\OpenMeteo\Traits\Pressure\SurfaceAirPressure;
use Flibidi67\OpenMeteo\Traits\Temperature\Temperature2m;
use Flibidi67\OpenMeteo\Traits\TotalColumnIntegratedWaterVapour;
use Flibidi67\OpenMeteo\Traits\WeatherCode;
use Flibidi67\OpenMeteo\Traits\Wind\Direction\WindDirection10m;
use Flibidi67\OpenMeteo\Traits\Wind\Speed\WindSpeed10m;

class Hourly extends AbstractHourly {
    use Temperature2m, PressureMSL, SurfaceAirPressure, WeatherCode, Precipitation, Snowfall, RunOff, TotalColumnIntegratedWaterVapour,
        CloudCover, CloudCoverLow, CloudCoverMid, CloudCoverHigh, WindSpeed10m, WindDirection10m, SkinTemperature, SoilTemperature0to7cm;

    protected array $availablesParameters = [
        self::TEMPERATURE_2M,
        self::PRESSURE_MSL,
        self::SURFACE_AIR_PRESSURE,
        self::WEATHERCODE,
        self::PRECIPITATION,
        self::SNOWFALL,
        self::RUN_OFF,
        self::TOTAL_COLUMN_INTEGRATED_WATER_VAPOUR,
        self::CLOUD_COVER,
        self::CLOUD_COVER_LOW,
        self::CLOUD_COVER_MID,
        self::CLOUD_COVER_HIGH,
        self::WIND_SPEED_10M,
        self::WIND_DIRECTION_10M,
        self::SKIN_TEMPERATURE,
        self::SOIL_TEMPERATURE_0_7CM
    ];

    public function __construct(AbstractOpenMeteoService $parent) {
        $this->pressureLevelClass = PressureLevel::class;
        parent::__construct($parent);
    }

    public function getHourly(): Hourly {
        return $this->parent->getHourly();
    }

    public function getDaily(): ?AbstractDaily {
        return $this->parent->getDaily();
    }
}