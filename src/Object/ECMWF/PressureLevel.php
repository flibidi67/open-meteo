<?php

namespace Flibidi67\OpenMeteo\Object\ECMWF;

use Flibidi67\OpenMeteo\Object\AbstractPressureLevel;

class PressureLevel extends AbstractPressureLevel {

    const TYPE_RELATIVE_HUMIDITY = "relative_humidity";
    const TYPES = [
        self::TYPE_TEMPERATURE,
        self::TYPE_RELATIVE_HUMIDITY,
        self::TYPE_SPECIFIC_HUMIDITY,
        self::TYPE_CLOUD_COVER,
        self::TYPE_WIND_SPEED,
        self::TYPE_WIND_DIRECTION,
        self::TYPE_GEOPOTENTIAL_HEIGHT,
        self::TYPE_DIVERGENCE_OF_WIND,
        self::TYPE_ATMOSPHERE_RELATIVE_VORTICITY
    ];

    const AVAILABLES_PRESSURES = [
        50,
        200,
        250,
        300,
        500,
        700,
        850,
        925,
        1000
    ];

    private string $type;
    private array $parameters = [];

    protected function getTypes(): array {
        return self::TYPES;
    }

    protected function getAvailablesPressures(): array {
        return self::AVAILABLES_PRESSURES;
    }
}