<?php

namespace Flibidi67\OpenMeteo\Traits\Pressure;

trait SurfaceAirPressure {
    const SURFACE_AIR_PRESSURE = "surface_air_pressure";

    /**
     * With surface air pressure
     * @return self
     */
    public function withSurfaceAirPressure(): self {
        return $this->with(self::SURFACE_AIR_PRESSURE);
    }
}