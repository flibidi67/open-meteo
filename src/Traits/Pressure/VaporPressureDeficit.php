<?php

namespace Flibidi67\OpenMeteo\Traits\Pressure;

trait VaporPressureDeficit {
    const VAPOR_PRESSURE_DEFICIT = "vapor_pressure_deficit";

    /**
     * With vapore pressure deficit
     * @return self
     */
    public function withVaporPressureDeficit(): self {
        return $this->with(self::VAPOR_PRESSURE_DEFICIT);
    }
}