<?php

namespace Flibidi67\OpenMeteo\Traits\Pressure;

trait SurfacePressure {
    const SURFACE_PRESSURE = "surface_pressure";

    /**
     * With surface pressure
     * @return self
     */
    public function withSurfacePressure(): self {
        return $this->with(self::SURFACE_PRESSURE);
    }
}