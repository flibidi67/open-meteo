<?php

namespace Flibidi67\OpenMeteo\Traits\Pressure;

trait PressureMSL {
    const PRESSURE_MSL = "pressure_msl";

    /**
     * With pressure MSL
     * @return self
     */
    public function withPressureMSL(): self {
        return $this->with(self::PRESSURE_MSL);
    }
}