<?php

namespace Flibidi67\OpenMeteo\Traits\Sun;

trait Sunset {
    const SUNSET = "sunset";

    /**
     * With sunset
     * @return self
     */
    public function withSunset(): self {
        return $this->with(self::SUNSET);
    }
}