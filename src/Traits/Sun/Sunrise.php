<?php

namespace Flibidi67\OpenMeteo\Traits\Sun;

trait Sunrise {
    const SUNRISE = "sunrise";

    /**
     * With sunrise
     * @return self
     */
    public function withSunrise(): self {
        return $this->with(self::SUNRISE);
    }
}