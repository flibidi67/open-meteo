<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait ShortWaveRadiationInstant {
    const SHORT_WAVE_RADIATION_INSTANT = "shortwave_radiation_instant";

    /**
     * With shortwave radiation instant
     * @return self
     */
    public function withShortWaveRadiationInstant(): self {
        return $this->with(self::SHORT_WAVE_RADIATION_INSTANT);
    }
}