<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait TerrestrialRadiationInstant {
    const TERRESTRIAL_RADIATION_INSTANT = "terrestrial_radiation_instant";

    /**
     * With terrestrial raditation instant
     * @return self
     */
    public function withTerrestrialRadiationInstant(): self {
        return $this->with(self::TERRESTRIAL_RADIATION_INSTANT);
    }
}