<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait DirectRadiation {
    const DIRECT_RADIATION = "direct_radiation";

    /**
     * With direct radiation
     * @return self
     */
    public function withDirectRadiation(): self {
        return $this->with(self::DIRECT_RADIATION);
    }
}