<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait DirectRadiationInstant {
    const DIRECT_RADIATION_INSTANT = "direct_radiation_instant";

    /**
     * With direct radiation instant
     * @return self
     */
    public function withDirectRaditationInstant(): self {
        return $this->with(self::DIRECT_RADIATION_INSTANT);
    }
}