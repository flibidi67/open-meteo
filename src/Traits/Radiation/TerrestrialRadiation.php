<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait TerrestrialRadiation {
    const TERRESTRIAL_RADIATION = "terrestrial_radiation";

    /**
     * With terrestrial radiation
     * @return self
     */
    public function withTerrestrialRadiation(): self {
        return $this->with(self::TERRESTRIAL_RADIATION);
    }
}