<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait DiffuseRadiationInstant {
    const DIFFUSE_RADIATION_INSTANT = "diffuse_radiation_instant";

    /**
     * With diffuse radiation instant
     * @return self
     */
    public function withDiffuseRaditationInstant(): self {
        return $this->with(self::DIFFUSE_RADIATION_INSTANT);
    }
}