<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait DirectNormalIrradiance {
    const DIRECT_NORMAL_IRRADIANCE = "direct_normal_irradiance";

    /**
     * With direct normal irradiance
     * @return self
     */
    public function withDirectNormalIrradiance(): self {
        return $this->with(self::DIRECT_NORMAL_IRRADIANCE);
    }
}