<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait DiffuseRadiation {
    const DIFFUSE_RADIATION = "diffuse_radiation";

    /**
     * With diffuse radiation
     * @return self
     */
    public function withDiffuseRadiation(): self {
        return $this->with(self::DIFFUSE_RADIATION);
    }
}