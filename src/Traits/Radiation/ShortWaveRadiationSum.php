<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait ShortWaveRadiationSum {
    const SHORT_WAVE_RADIATION_SUM = "shortwave_radiation_sum";

    /**
     * With shortwave radiation sum
     */
    public function withShortWaveRadiationSum(): self {
        return $this->with(self::SHORT_WAVE_RADIATION_SUM);
    }
}