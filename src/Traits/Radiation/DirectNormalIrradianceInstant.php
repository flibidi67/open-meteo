<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait DirectNormalIrradianceInstant {
    const DIRECT_NORMAL_IRRADIANCE_INSTANT = "direct_normal_irradiance_instant";

    /**
     * With diffuse normal radiation instant
     * @return self
     */
    public function withDirectNormalIrradianceInstant(): self {
        return $this->with(self::DIRECT_NORMAL_IRRADIANCE_INSTANT);
    }
}