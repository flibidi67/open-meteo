<?php

namespace Flibidi67\OpenMeteo\Traits\Radiation;

trait ShortWaveRadiation {
    const SHORT_WAVE_RADIATION = "shortwave_radiation";

    /**
     * With shortwave radiation
     * @return self
     */
    public function withShortWaveRadiation(): self {
        return $this->with(self::SHORT_WAVE_RADIATION);
    }
}