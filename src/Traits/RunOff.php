<?php

namespace Flibidi67\OpenMeteo\Traits;

trait RunOff {
    const RUN_OFF = "runoff";

    /**
     * With run off
     * @return self
     */
    public function withRunOff(): self {
        return $this->with(self::RUN_OFF);
    }
}