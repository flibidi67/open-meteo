<?php

namespace Flibidi67\OpenMeteo\Traits;

trait TotalColumnIntegratedWaterVapour {
    const TOTAL_COLUMN_INTEGRATED_WATER_VAPOUR = "total_column_integrated_water_vapour";

    /**
     * With total column integrated water vapour
     * @return self
     */
    public function withTotalColumnIntegratedWaterVapour(): self {
        return $this->with(self::TOTAL_COLUMN_INTEGRATED_WATER_VAPOUR);
    }
}