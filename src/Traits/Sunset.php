<?php

namespace Flibidi67\OpenMeteo\Traits;

trait Sunset {
    const SUNSET = "sunset";

    /**
     * With sunset
     */
    public function withSunset(): self {
        return $this->with(self::SUNSET);
    }
}