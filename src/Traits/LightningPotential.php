<?php

namespace Flibidi67\OpenMeteo\Traits;

trait LightningPotential {
    const LIGHTNING_POTENTIAL = "lightning_potential";

    /**
     * With lightning potential
     * @return self
     */
    public function withLightningPotential(): self {
        return $this->with(self::LIGHTNING_POTENTIAL);
    }
}