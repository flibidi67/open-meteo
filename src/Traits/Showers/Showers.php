<?php

namespace Flibidi67\OpenMeteo\Traits\Showers;

trait Showers {
    const SHOWERS = "showers";

    /**
     * With showers
     * @return self
     */
    public function withShowers(): self {
        return $this->with(self::SHOWERS);
    }
}