<?php

namespace Flibidi67\OpenMeteo\Traits\Showers;

trait ShowersSum {
    const SHOWERS_SUM = "showers_sum";

    /**
     * With showers sum
     */
    public function withShowersSum(): self {
        return $this->with(self::SHOWERS_SUM);
    }
}