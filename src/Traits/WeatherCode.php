<?php

namespace Flibidi67\OpenMeteo\Traits;

trait WeatherCode {
    const WEATHERCODE = "weathercode";

    /**
     * With weathercode
     * @return self
     */
    public function withWeatherCode(): self {
        return $this->with(self::WEATHERCODE);
    }
}