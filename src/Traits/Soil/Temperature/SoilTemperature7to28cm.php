<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature7to28cm {
    const SOIL_TEMPERATURE_7_28CM = "soil_temperature_7_to_28cm";

    /**
     * With soil temperature 7cm to 28cm
     * @return self
     */
    public function withSoilTemperature7To28cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_7_28CM);
    }
}