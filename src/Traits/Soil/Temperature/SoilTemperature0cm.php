<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature0cm {
    const SOIL_TEMPERATURE_0CM = "soil_temperature_0cm";

    /**
     * With soil temperature 0cm
     * @return self
     */
    public function withSoilTemperature0cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_0CM);
    }
}