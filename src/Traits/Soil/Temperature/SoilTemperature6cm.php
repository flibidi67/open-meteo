<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature6cm {
    const SOIL_TEMPERATURE_6CM = "soil_temperature_6cm";

    /**
     * With soil temperature 6cm
     * @return self
     */
    public function withSoilTemperature6cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_6CM);
    }
}