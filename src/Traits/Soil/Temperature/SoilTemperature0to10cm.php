<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature0to10cm {
    const SOIL_TEMPERATURE_0_10CM = "soil_temperature_0_to_10cm";

    /**
     * With soil temperature 0cm to 10cm
     * @return self
     */
    public function withSoilTemperature0To10cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_0_10CM);
    }
}