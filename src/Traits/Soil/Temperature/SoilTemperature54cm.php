<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature54cm {
    const SOIL_TEMPERATURE_54CM = "soil_temperature_54cm";

    /**
     * With soil temperature 54cm
     * @return self
     */
    public function withSoilTemperature54cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_54CM);
    }
}