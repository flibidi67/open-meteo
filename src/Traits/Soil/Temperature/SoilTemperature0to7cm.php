<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature0to7cm {
    const SOIL_TEMPERATURE_0_7CM = "soil_temperature_0_to_7cm";

    /**
     * With soil temperature 0cm to 7cm
     * @return self
     */
    public function withSoilTemperature0To7cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_0_7CM);
    }
}