<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature40to100cm {
    const SOIL_TEMPERATURE_40_100CM = "soil_temperature_40_to_100cm";

    /**
     * With soil temperature 40cm to 100cm
     * @return self
     */
    public function withSoilTemperature40To100cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_40_100CM);
    }
}