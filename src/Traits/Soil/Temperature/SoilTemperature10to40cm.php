<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature10to40cm {
    const SOIL_TEMPERATURE_10_40CM = "soil_temperature_10_to_40cm";

    /**
     * With soil temperature 10cm to 40cm
     * @return self
     */
    public function withSoilTemperature10To40cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_10_40CM);
    }
}