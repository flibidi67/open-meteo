<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature100to255cm {
    const SOIL_TEMPERATURE_100_255CM = "soil_temperature_100_to_255cm";

    /**
     * With soil temperature 100cm to 255cm
     * @return self
     */
    public function withSoilTemperature100To255cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_100_255CM);
    }
}