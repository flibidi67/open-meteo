<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature18cm {
    const SOIL_TEMPERATURE_18CM = "soil_temperature_18cm";

    /**
     * With soil temperature 18cm
     * @return self
     */
    public function withSoilTemperature18cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_18CM);
    }
}