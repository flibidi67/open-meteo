<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature28to100cm {
    const SOIL_TEMPERATURE_28_100CM = "soil_temperature_28_to_100cm";

    /**
     * With soil temperature 28cm to 100cm
     * @return self
     */
    public function withSoilTemperature28To100cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_28_100CM);
    }
}