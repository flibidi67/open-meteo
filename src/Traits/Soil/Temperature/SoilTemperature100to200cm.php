<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Temperature;

trait SoilTemperature100to200cm {
    const SOIL_TEMPERATURE_100_200CM = "soil_temperature_100_to_200cm";

    /**
     * With soil temperature 100cm to 200cm
     * @return self
     */
    public function withSoilTemperature100To200cm(): self {
        return $this->with(self::SOIL_TEMPERATURE_100_200CM);
    }
}