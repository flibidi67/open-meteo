<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture100to255cm {
    const SOIL_MOISTURE_100_255CM = "soil_moisture_100_to_255cm";

    /**
     * With soil moisture 100 to 255cm
     * @return self
     */
    public function withSoilMoisture100To255cm(): self {
        return $this->with(self::SOIL_MOISTURE_100_255CM);
    }
}