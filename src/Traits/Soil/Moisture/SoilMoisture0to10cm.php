<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture0to10cm {
    const SOIL_MOISTURE_0_10CM = "soil_moisture_0_to_10cm";

    /**
     * With soil moisture 0 to 10cm
     * @return self
     */
    public function withSoilMoisture0To10cm(): self {
        return $this->with(self::SOIL_MOISTURE_0_10CM);
    }
}