<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture0to7cm {
    const SOIL_MOISTURE_0_7CM = "soil_moisture_0_to_7cm";

    /**
     * With soil moisture 0 to 7cm
     * @return self
     */
    public function withSoilMoisture0To7cm(): self {
        return $this->with(self::SOIL_MOISTURE_0_7CM);
    }
}