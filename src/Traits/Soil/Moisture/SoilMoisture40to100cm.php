<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture40to100cm {
    const SOIL_MOISTURE_40_100CM = "soil_moisture_40_to_100cm";

    /**
     * With soil moisture 40 to 100cm
     * @return self
     */
    public function withSoilMoisture40To100cm(): self {
        return $this->with(self::SOIL_MOISTURE_40_100CM);
    }
}