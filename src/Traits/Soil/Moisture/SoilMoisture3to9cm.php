<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture3to9cm {
    const SOIL_MOISTURE_3_9CM = "soil_moisture_3_9cm";

    /**
     * With soil moisture 3 to 9cm
     * @return self
     */
    public function withSoilMoisture3To9cm(): self {
        return $this->with(self::SOIL_MOISTURE_3_9CM);
    }
}