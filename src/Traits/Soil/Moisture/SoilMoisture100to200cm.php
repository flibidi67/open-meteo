<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture100to200cm {
    const SOIL_MOISTURE_100_200CM = "soil_moisture_100_to_200cm";

    /**
     * With soil moisture 100 to 200cm
     * @return self
     */
    public function withSoilMoisture100To200cm(): self {
        return $this->with(self::SOIL_MOISTURE_100_200CM);
    }
}