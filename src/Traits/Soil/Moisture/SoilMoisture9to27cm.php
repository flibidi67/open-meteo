<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture9to27cm {
    const SOIL_MOISTURE_9_27CM = "soil_moisture_9_27cm";

    /**
     * With soil moisture 9 to 27cm
     * @return self
     */
    public function withSoilMoisture9To27cm(): self {
        return $this->with(self::SOIL_MOISTURE_9_27CM);
    }
}