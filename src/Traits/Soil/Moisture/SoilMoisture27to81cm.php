<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture27to81cm {
    const SOIL_MOISTURE_27_81CM = "soil_moisture_27_81cm";

    /**
     * With soil moisture 27 to 81cm
     * @return self
     */
    public function withSoilMoisture27To81cm(): self {
        return $this->with(self::SOIL_MOISTURE_27_81CM);
    }
}