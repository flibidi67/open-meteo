<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture10to40cm {
    const SOIL_MOISTURE_10_40CM = "soil_moisture_10_to_40cm";

    /**
     * With soil moisture 10 to 40cm
     * @return self
     */
    public function withSoilMoisture10To40cm(): self {
        return $this->with(self::SOIL_MOISTURE_10_40CM);
    }
}