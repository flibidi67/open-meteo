<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture28to100cm {
    const SOIL_MOISTURE_28_100CM = "soil_moisture_28_to_100cm";

    /**
     * With soil moisture 28 to 100cm
     * @return self
     */
    public function withSoilMoisture28To100cm(): self {
        return $this->with(self::SOIL_MOISTURE_28_100CM);
    }
}