<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture7to28cm {
    const SOIL_MOISTURE_7_28CM = "soil_moisture_7_to_28cm";

    /**
     * With soil moisture 7 to 28cm
     * @return self
     */
    public function withSoilMoisture7To28cm(): self {
        return $this->with(self::SOIL_MOISTURE_7_28CM);
    }
}