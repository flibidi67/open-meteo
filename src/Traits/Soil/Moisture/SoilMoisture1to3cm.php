<?php

namespace Flibidi67\OpenMeteo\Traits\Soil\Moisture;

trait SoilMoisture1to3cm {
    const SOIL_MOISTURE_1_3CM = "soil_moisture_1_3cm";

    /**
     * With soil moisture 1 to 3cm
     * @return self
     */
    public function withSoilMoisture1To3cm(): self {
        return $this->with(self::SOIL_MOISTURE_1_3CM);
    }
}