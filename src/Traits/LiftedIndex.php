<?php

namespace Flibidi67\OpenMeteo\Traits;

trait LiftedIndex {
    const LIFTED_INDEX = "lifted_index";

    /**
     * With lifted index
     * @return self
     */
    public function withLiftedIndex(): self {
        return $this->with(self::LIFTED_INDEX);
    }
}