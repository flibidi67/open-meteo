<?php

namespace Flibidi67\OpenMeteo\Traits;

trait CAPE {
    const CAPE = "cape";

    /**
     * With CAPE
     * @return self
     */
    public function withCAPE(): self {
        return $this->with(self::CAPE);
    }
}