<?php

namespace Flibidi67\OpenMeteo\Traits\Temperature;

trait Temperature120m {
    const TEMPERATURE_120M = "temperature_120m";

    /**
     * With temperature 120m
     * @return self
     */
    public function withTemperature120m(): self {
        return $this->with(self::TEMPERATURE_120M);
    }
}