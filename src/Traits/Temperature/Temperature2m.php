<?php

namespace Flibidi67\OpenMeteo\Traits\Temperature;

trait Temperature2m {
    const TEMPERATURE_2M = "temperature_2m";

    /**
     * With temperature 2m
     * @return self
     */
    public function withTemperature2m(): self {
        return $this->with(self::TEMPERATURE_2M);
    }
}