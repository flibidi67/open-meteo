<?php

namespace Flibidi67\OpenMeteo\Traits\Temperature;

trait Temperature180m {
    const TEMPERATURE_180M = "temperature_180m";

    /**
     * With temperature 180m
     * @return self
     */
    public function withTemperature180m(): self {
        return $this->with(self::TEMPERATURE_180M);
    }
}