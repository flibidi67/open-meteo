<?php

namespace Flibidi67\OpenMeteo\Traits\Temperature;

trait Temperature2mMin {
    const TEMPERATURE_2M_MIN = "temperature_2m_min";

    /**
     * With temperature 2m min
     * @return self
     */
    public function withTemperature2mMin(): self {
        return $this->with(self::TEMPERATURE_2M_MIN);
    }
}