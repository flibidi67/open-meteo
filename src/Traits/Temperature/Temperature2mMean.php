<?php

namespace Flibidi67\OpenMeteo\Traits\Temperature;

trait Temperature2mMean {
    const TEMPERATURE_2M_MEAN = "temperature_2m_mean";

    /**
     * With temperature 2m mean
     * @return self
     */
    public function withTemperature2mMean(): self {
        return $this->with(self::TEMPERATURE_2M_MEAN);
    }
}