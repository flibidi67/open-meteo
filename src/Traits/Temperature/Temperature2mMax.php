<?php

namespace Flibidi67\OpenMeteo\Traits\Temperature;

trait Temperature2mMax {
    const TEMPERATURE_2M_MAX = "temperature_2m_max";

    /**
     * With temperature 2m max
     * @return self
     */
    public function withTemperature2mMax(): self {
        return $this->with(self::TEMPERATURE_2M_MAX);
    }
}