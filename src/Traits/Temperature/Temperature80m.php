<?php

namespace Flibidi67\OpenMeteo\Traits\Temperature;

trait Temperature80m {
    const TEMPERATURE_80M = "temperature_80m";

    /**
     * With temperature 80m
     * @return self
     */
    public function withTemperature80m(): self {
        return $this->with(self::TEMPERATURE_80M);
    }
}