<?php

namespace Flibidi67\OpenMeteo\Traits\Temperature;

trait Temperature40m {
    const TEMPERATURE_40M = "temperature_40m";

    /**
     * With temperature 40m
     * @return self
     */
    public function withTemperature40m(): self {
        return $this->with(self::TEMPERATURE_40M);
    }
}