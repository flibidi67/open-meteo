<?php

namespace Flibidi67\OpenMeteo\Traits;

trait DewPoint2m {
    const DEW_POINT_2M = "dewpoint_2m";

    /**
     * With dew point 2m
     * @return self
     */
    public function withDewPoint2m(): self {
        return $this->with(self::DEW_POINT_2M);
    }
}