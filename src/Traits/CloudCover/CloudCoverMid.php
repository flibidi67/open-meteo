<?php

namespace Flibidi67\OpenMeteo\Traits\CloudCover;

trait CloudCoverMid {
    const CLOUD_COVER_MID = "cloudcover_mid";

    /**
     * With cloud cover mid
     * @return self
     */
    public function withCloudCoverMid(): self {
        return $this->with(self::CLOUD_COVER_MID);
    }
}