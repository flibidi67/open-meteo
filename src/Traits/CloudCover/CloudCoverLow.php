<?php

namespace Flibidi67\OpenMeteo\Traits\CloudCover;

trait CloudCoverLow {
    const CLOUD_COVER_LOW = "cloudcover_low";

    /**
     * With cloud cover low
     * @return self
     */
    public function withCloudCoverLow(): self {
        return $this->with(self::CLOUD_COVER_LOW);
    }
}