<?php

namespace Flibidi67\OpenMeteo\Traits\CloudCover;

trait CloudCoverHigh {
    const CLOUD_COVER_HIGH = "cloudcover_high";

    /**
     * With cloud cover high
     * @return self
     */
    public function withCloudCoverHigh(): self {
        return $this->with(self::CLOUD_COVER_HIGH);
    }
}