<?php

namespace Flibidi67\OpenMeteo\Traits\CloudCover;

trait CloudCover {
    const CLOUD_COVER = "cloudcover";

    /**
     * With cloud cover
     * @return self
     */
    public function withCloudCover(): self {
        return $this->with(self::CLOUD_COVER);
    }
}