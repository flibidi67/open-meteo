<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Direction;

trait WindDirection40m {
    const WIND_DIRECTION_40M = "winddirection_40m";

    /**
     * With wind direction 40m
     * @return self
     */
    public function withWindDirection40m(): self {
        return $this->with(self::WIND_DIRECTION_40M);
    }
}