<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Direction;

trait WindDirection120m {
    const WIND_DIRECTION_120M = "winddirection_120m";

    /**
     * With wind direction 120m
     * @return self
     */
    public function withWindDirection120m(): self {
        return $this->with(self::WIND_DIRECTION_120M);
    }
}