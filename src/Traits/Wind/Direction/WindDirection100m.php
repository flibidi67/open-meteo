<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Direction;

trait WindDirection100m {
    const WIND_DIRECTION_100M = "winddirection_100m";

    /**
     * With wind direction 100m
     * @return self
     */
    public function withWindDirection100m(): self {
        return $this->with(self::WIND_DIRECTION_100M);
    }
}