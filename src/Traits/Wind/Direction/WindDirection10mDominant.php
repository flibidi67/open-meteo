<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Direction;

trait WindDirection10mDominant {
    const WIND_DIRECTION_10M_DOMINANT = "winddirection_10m_dominant";

    /**
     * With wind direction 10m dominant
     */
    public function withWindDirection10mDominant(): self {
        return $this->with(self::WIND_DIRECTION_10M_DOMINANT);
    }
}