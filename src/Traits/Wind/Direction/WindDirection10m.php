<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Direction;

trait WindDirection10m {
    const WIND_DIRECTION_10M = "winddirection_10m";

    /**
     * With wind direction 10m
     * @return self
     */
    public function withWindDirection10m(): self {
        return $this->with(self::WIND_DIRECTION_10M);
    }
}