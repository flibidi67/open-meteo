<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Direction;

trait WindDirection80m {
    const WIND_DIRECTION_80M = "winddirection_80m";

    /**
     * With wind direction 80m
     * @return self
     */
    public function withWindDirection80m(): self {
        return $this->with(self::WIND_DIRECTION_80M);
    }
}