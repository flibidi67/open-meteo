<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Direction;

trait WindDirection180m {
    const WIND_DIRECTION_180M = "winddirection_180m";

    /**
     * With wind direction 180m
     * @return self
     */
    public function withWindDirection180m(): self {
        return $this->with(self::WIND_DIRECTION_180M);
    }
}