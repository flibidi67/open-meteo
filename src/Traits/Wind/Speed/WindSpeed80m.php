<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Speed;

trait WindSpeed80m {
    const WIND_SPEED_80M = "windspeed_80m";

    /**
     * With wind speed 80m
     * @return self
     */
    public function withWindSpeed80m(): self {
        return $this->with(self::WIND_SPEED_80M);
    }
}