<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Speed;

trait WindSpeed120m {
    const WIND_SPEED_120M = "windspeed_120m";

    /**
     * With wind speed 120m
     * @return self
     */
    public function withWindSpeed120m(): self {
        return $this->with(self::WIND_SPEED_120M);
    }
}