<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Speed;

trait WindSpeed10m {
    const WIND_SPEED_10M = "windspeed_10m";

    /**
     * With wind speed 10m
     * @return self
     */
    public function withWindSpeed10m(): self {
        return $this->with(self::WIND_SPEED_10M);
    }
}