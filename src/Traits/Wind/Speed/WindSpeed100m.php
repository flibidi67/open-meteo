<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Speed;

trait WindSpeed100m {
    const WIND_SPEED_100M = "windspeed_100m";

    /**
     * With wind speed 100m
     * @return self
     */
    public function withWindSpeed100m(): self {
        return $this->with(self::WIND_SPEED_100M);
    }
}