<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Speed;

trait WindSpeed40m {
    const WIND_SPEED_40M = "windspeed_40m";

    /**
     * With wind speed 40m
     * @return self
     */
    public function withWindSpeed40m(): self {
        return $this->with(self::WIND_SPEED_40M);
    }
}