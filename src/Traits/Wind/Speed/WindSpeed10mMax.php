<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Speed;

trait WindSpeed10mMax {
    const WIND_SPEED_10M_MAX = "windspeed_10m_max";

    /**
     * With windspeed 10m max
     */
    public function withWindSpeed10mMax(): self {
        return $this->with(self::WIND_SPEED_10M_MAX);
    }
}