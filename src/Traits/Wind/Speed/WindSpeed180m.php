<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Speed;

trait WindSpeed180m {
    const WIND_SPEED_180M = "windspeed_180m";

    /**
     * With wind speed 180m
     * @return self
     */
    public function withWindSpeed180m(): self {
        return $this->with(self::WIND_SPEED_180M);
    }
}