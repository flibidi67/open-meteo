<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Gusts;

trait WindGusts10mMax {
    const WIND_GUSTS_10M_MAX = "windgusts_10m_max";

    /**
     * With windgusts 10m max
     */
    public function withWindGusts10mMax(): self {
        return $this->with(self::WIND_GUSTS_10M_MAX);
    }
}