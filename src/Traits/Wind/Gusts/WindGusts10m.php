<?php

namespace Flibidi67\OpenMeteo\Traits\Wind\Gusts;

trait WindGusts10m {
    const WIND_GUSTS_10M = "windgusts_10m";

    /**
     * With gusts 10m
     * @return self
     */
    public function withWindGusts10m(): self {
        return $this->with(self::WIND_GUSTS_10M);
    }
}