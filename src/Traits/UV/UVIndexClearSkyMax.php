<?php

namespace Flibidi67\OpenMeteo\Traits\UV;

trait UVIndexClearSkyMax {
    const UV_INDEX_CLEAR_SKY_MAX = "uv_index_clear_sky_max";

    /**
     * With uv index clear sky max
     */
    public function withUVIndexClearSkyMax(): self {
        return $this->with(self::UV_INDEX_CLEAR_SKY_MAX);
    }
}