<?php

namespace Flibidi67\OpenMeteo\Traits\UV;

trait UVIndex {
    const UV_INDEX = "uv_index";

    /**
     * With uv index
     * @return self
     */
    public function withUVIndex(): self {
        return $this->with(self::UV_INDEX);
    }
}