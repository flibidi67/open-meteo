<?php

namespace Flibidi67\OpenMeteo\Traits\UV;

trait UVIndexMax {
    const UV_INDEX_MAX = "uv_index_max";

    /**
     * With uv index max
     */
    public function withUVIndexMax(): self {
        return $this->with(self::UV_INDEX_MAX);
    }
}