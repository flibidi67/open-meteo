<?php

namespace Flibidi67\OpenMeteo\Traits\UV;

trait UVIndexClearSky {
    const UV_INDEX_CLEAR_SKY = "uv_index_clear_sky";

    /**
     * With uv index clear sky
     * @return self
     */
    public function withUVIndexClearSky(): self {
        return $this->with(self::UV_INDEX_CLEAR_SKY);
    }
}