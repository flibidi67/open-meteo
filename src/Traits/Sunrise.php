<?php

namespace Flibidi67\OpenMeteo\Traits;

trait Sunrise {
    const SUNRISE = "sunrise";

    /**
     * With sunrise
     */
    public function withSunrise(): self {
        return $this->with(self::SUNRISE);
    }
}