<?php

namespace Flibidi67\OpenMeteo\Traits;

trait SkinTemperature {
    const SKIN_TEMPERATURE = "skin_temperature";

    /**
     * With skin temperature
     * @return self
     */
    public function withSkinTemperature(): self {
        return $this->with(self::SKIN_TEMPERATURE);
    }
}