<?php

namespace Flibidi67\OpenMeteo\Traits;

trait RelativeHumidity2m {
    const RELATIVE_HUMIDITY_2M = "relativehumidity_2m";

    /**
     * With relative humidity 2m
     * @return self
     */
    public function withRelativeHumidity2m(): self {
        return $this->with(self::RELATIVE_HUMIDITY_2M);
    }
}