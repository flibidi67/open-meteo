<?php

namespace Flibidi67\OpenMeteo\Traits;

trait FreezingLevelHeight {
    const FREEZING_LEVEL_HEIGHT = "freezinglevel_height";

    /**
     * With freezing level height
     * @return self
     */
    public function withFreezingLevelHeight(): self {
        return $this->with(self::FREEZING_LEVEL_HEIGHT);
    }
}