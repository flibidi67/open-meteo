<?php

namespace Flibidi67\OpenMeteo\Traits\Evapotranspiration;

trait Et0FaoEvapotranspiration {
    const ET0_FAO_EVAPOTRANSPIRATION = "et0_fao_evapotranspiration";

    /**
     * With et0 fao evapotranspiration
     */
    public function withEt0FaoEvapotranspiration(): self {
        return $this->with(self::ET0_FAO_EVAPOTRANSPIRATION);
    }
}