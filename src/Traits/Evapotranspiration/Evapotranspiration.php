<?php

namespace Flibidi67\OpenMeteo\Traits\Evapotranspiration;

trait Evapotranspiration {
    const EVAPOTRANSPIRATION = "evapotranspiration";

    /**
     * With evapotranspiration
     * @return self
     */
    public function withEvapotranspiration(): self {
        return $this->with(self::EVAPOTRANSPIRATION);
    }
}