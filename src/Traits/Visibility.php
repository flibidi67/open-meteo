<?php

namespace Flibidi67\OpenMeteo\Traits;

trait Visibility {
    const VISIBILITY = "visibility";

    /**
     * With visibility
     * @return self
     */
    public function withVisibility(): self {
        return $this->with(self::VISIBILITY);
    }
}