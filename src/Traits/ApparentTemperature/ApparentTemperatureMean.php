<?php

namespace Flibidi67\OpenMeteo\Traits\ApparentTemperature;

trait ApparentTemperatureMean {
    const APPARENT_TEMPERATURE_MEAN = "apparent_temperature_mean";

    /**
     * With apparent temperature mean
     * @return self
     */
    public function withApparentTemperatureMean(): self {
        return $this->with(self::APPARENT_TEMPERATURE_MEAN);
    }
}