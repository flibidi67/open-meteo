<?php

namespace Flibidi67\OpenMeteo\Traits\ApparentTemperature;

trait ApparentTemperature {
    const APPARENT_TEMPERATURE = "apparent_temperature";

    /**
     * With apparent temperature
     * @return self
     */
    public function withApparentTemperature(): self {
        return $this->with(self::APPARENT_TEMPERATURE);
    }
}