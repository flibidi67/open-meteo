<?php

namespace Flibidi67\OpenMeteo\Traits\ApparentTemperature;

trait ApparentTemperatureMin {
    const APPARENT_TEMPERATURE_MIN = "apparent_temperature_min";

    /**
     * With apparent temperature min
     * @return self
     */
    public function withApparentTemperatureMin(): self {
        return $this->with(self::APPARENT_TEMPERATURE_MIN);
    }
}