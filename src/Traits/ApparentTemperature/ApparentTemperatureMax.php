<?php

namespace Flibidi67\OpenMeteo\Traits\ApparentTemperature;

trait ApparentTemperatureMax {
    const APPARENT_TEMPERATURE_MAX = "apparent_temperature_max";

    /**
     * With apparent temperature max
     * @return self
     */
    public function withApparentTemperatureMax(): self {
        return $this->with(self::APPARENT_TEMPERATURE_MAX);
    }
}