<?php

namespace Flibidi67\OpenMeteo\Traits\Precipitation;

trait PrecipitationSum {
    const PRECIPITATION_SUM = "precipitation_sum";

    /**
     * With precipitation sum
     */
    public function withPrecipitationSum(): self {
        return $this->with(self::PRECIPITATION_SUM);
    }
}