<?php

namespace Flibidi67\OpenMeteo\Traits\Precipitation;

trait PrecipitationProbability {
    const PRECIPITATION_PROBABILITY = "precipitation_probability";

    /**
     * With precipitation probability
     * @return self
     */
    public function withPrecipitationProbability(): self {
        return $this->with(self::PRECIPITATION_PROBABILITY);
    }
}