<?php

namespace Flibidi67\OpenMeteo\Traits\Precipitation;

trait PrecipitationProbabilityMax {
    const PRECIPITATION_PROBABILITY_MAX = "precipitation_probability_max";

    /**
     * With precipitation probability max
     */
    public function withPrecipitationProbabilityMax(): self {
        return $this->with(self::PRECIPITATION_PROBABILITY_MAX);
    }
}