<?php

namespace Flibidi67\OpenMeteo\Traits\Precipitation;

trait Precipitation {
    const PRECIPITATION = "precipitation";

    /**
     * With precipitation
     * @return self
     */
    public function withPrecipitation(): self {
        return $this->with(self::PRECIPITATION);
    }
}