<?php

namespace Flibidi67\OpenMeteo\Traits\Precipitation;

trait PrecipitationHours {
    const PRECIPITATION_HOURS = "precipitation_hours";

    /**
     * With precipitation hours
     */
    public function withPrecipitationHours(): self {
        return $this->with(self::PRECIPITATION_HOURS);
    }
}