<?php

namespace Flibidi67\OpenMeteo\Traits\Rain;

trait RainSum {
    const RAIN_SUM = "rain_sum";

    /**
     * With rain sum
     */
    public function withRainSum(): self {
        return $this->with(self::RAIN_SUM);
    }
}