<?php

namespace Flibidi67\OpenMeteo\Traits\Rain;

trait Rain {
    const RAIN = "rain";

    /**
     * With rain
     * @return self
     */
    public function withRain(): self {
        return $this->with(self::RAIN);
    }
}