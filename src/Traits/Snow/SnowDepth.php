<?php

namespace Flibidi67\OpenMeteo\Traits\Snow;

trait SnowDepth {
    const SNOW_DEPTH = "snow_depth";

    /**
     * With snow depth
     * @return self
     */
    public function withSnowDepth(): self {
        return $this->with(self::SNOW_DEPTH);
    }
}