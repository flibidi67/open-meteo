<?php

namespace Flibidi67\OpenMeteo\Traits\Snow;

trait Snowfall {
    const SNOWFALL = "snowfall";

    /**
     * With snowfall
     * @return self
     */
    public function withSnowfall(): self {
        return $this->with(self::SNOWFALL);
    }
}