<?php

namespace Flibidi67\OpenMeteo\Traits\Snow;

trait SnowfallHeight {
    const SNOWFALL_HEIGHT = "snowfall_height";

    /**
     * With snowfall height
     * @return self
     */
    public function withSnowfallHeight(): self {
        return $this->with(self::SNOWFALL_HEIGHT);
    }
}