<?php

namespace Flibidi67\OpenMeteo\Traits\Snow;

trait SnowfallSum {
    const SNOWFALL_SUM = "snowfall_sum";

    /**
     * With snowfall sum
     */
    public function withSnowfallSum(): self {
        return $this->with(self::SNOWFALL_SUM);
    }
}