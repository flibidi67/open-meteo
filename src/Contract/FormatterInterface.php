<?php

namespace Flibidi67\OpenMeteo\Contract;

/**
 * @see \Flibidi67\OpenMeteo\Formatter\DefaultFormatter for an example of usage.
 */
interface FormatterInterface {
    public function format(array $data): array;
}