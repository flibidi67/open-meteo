<?php

namespace Flibidi67\OpenMeteo\DependencyInjection;

use Flibidi67\OpenMeteo\Object\Settings;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface {

    public function getConfigTreeBuilder(): TreeBuilder {
        // All the available settings fields.
        $fields = [
            'temperature_unit' => [
                'default' => Settings::DEFAULT_TEMPERATURE_UNIT,
                'accept' => Settings::TEMPERATURE_UNITS,
                'invalid_message' => 'Invalid temperature_unit %s'
            ],
            'wind_speed_unit' => [
                'default' => Settings::DEFAULT_WIND_SPEED_UNIT,
                'accept' => Settings::WIND_SPEED_UNITS,
                'invalid_message' => 'Invalid wind_speed_unit %s'
            ],
            'precipitation_unit' => [
                'default' => Settings::DEFAULT_PRECIPITATION_UNIT,
                'accept' => Settings::PRECIPITATION_UNITS,
                'invalid_message' => 'Invalid precipitation_unit %s'
            ],
            'timeformat' => [
                'default' => Settings::DEFAULT_TIMEFORMAT,
                'accept' => Settings::TIMEFORMATS,
                'invalid_message' => 'Invalid timeformat %s'
            ],
            'timezone' => [
                'default' => Settings::DEFAULT_TIMEZONE,
                'accept' => array_merge(['auto', 'GMT'], timezone_identifiers_list()),
                'invalid_message' => 'Invalid timezone %s'
            ],
            'past_days' => [
                'default' => Settings::DEFAULT_PAST_DAYS,
                'accept' => range(Settings::MIN_PAST_DAYS, Settings::MAX_PAST_DAYS),
                'invalid_message' => 'Invalid past_days %s'
            ],
            'forecast_days' => [
                'default' => Settings::FORECAST_DAYS,
                'accept' => range(Settings::MIN_FORECAST_DAYS, Settings::MAX_FORECAST_DAYS),
                'invalid_message' => 'Invalid forecast_days %s'
            ],
            'current_weather' => [
                'default' => Settings::CURRENT_WEATHER,
                'accept' => [true, false],
                'invalid_message' => 'Invalid current_weather %s'
            ]
        ];

        $apis = [
            'default' => [],
            'forecast' => [],
            'historical' => ['past_days', 'current_weather', 'forecast_days'],
            'ecmwf' => ['forecast_days', 'timezone', 'current_weather'],
            'gfs' => [],
            'meteofrance' => ['forecast_days'],
            'dwd' => ['forecast_days'],
            'jma' => ['forecast_days'],
            'metno' => ['forecast_days'],
            'gem' => ['forecast_days']
        ];

        $treeBuilder = new TreeBuilder('flibidi67_open_meteo');
        $baseChildren = $treeBuilder->getRootNode()->children();

        foreach ($apis as $api => $excludedFields) {
            $currentChildren = $baseChildren->arrayNode($api)->addDefaultsIfNotSet()->children();
            foreach ($fields as $field => $config) {
                if (in_array($field, $excludedFields)) {
                    continue;
                }

                if ($api !== 'default') {
                    $config['accept'][] = null;
                }

                $currentChildren
                    ->scalarNode($field)
                    ->defaultValue($config['default'])
                    ->validate()
                        ->ifNotInArray($config['accept'])
                        ->thenInvalid($config['invalid_message'])
                    ->end();
            }
            $currentChildren->end();
        }

        return $treeBuilder;
    }
}