<?php

namespace Flibidi67\OpenMeteo\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

final class Flibidi67OpenMeteoExtension extends Extension {
    
    public function load(array $configs, ContainerBuilder $containerBuilder) {
        $loader = new YamlFileLoader(
            $containerBuilder,
            new FileLocator(__DIR__ . "/../../config")
        );
        $loader->load('services.yaml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        // Here we overwrite the specific config with null values with the default values.
        $defaultConfig = $config['default'];
        foreach ($config as $api => &$fields) {
            if ($api === 'default') {
                continue;
            }
            foreach ($fields as $field => &$value) {
                if (is_null($value)) {
                    $value = $defaultConfig[$field];
                }
            }
        }

        foreach ($config as $api => $parameters) {
            $containerBuilder->setParameter("flibidi67_open_meteo.$api", $parameters);
        }
    }
}