# 🌤 Open-Meteo API

[![Latest Version on Packagist](https://img.shields.io/packagist/v/flibidi67/open-meteo.svg?style=flat-square)](https://packagist.org/packages/flibidi67/open-meteo)
[![Total Downloads](https://img.shields.io/packagist/dt/flibidi67/open-meteo.svg?style=flat-square)](https://packagist.org/packages/flibidi67/open-meteo)

[![Open-Meteo](https://cdn.substack.com/image/fetch/w_1360,c_limit,f_auto,q_auto:best,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Ffd0d7953-5a9d-441c-b59f-4cde244503a1_934x461.png)](https://open-meteo.com/en)

Open-Meteo collaborates with National Weather Services providing Open Data with 11 to 2 km resolution. Our high performance APIs select the best weather model for your location and provide data as a simple JSON API.

APIs are free without any API key for open-source developers and non-commercial use. You can embed them directly into your app.

## Installation

You can install the package via composer:
```bash
composer require flibidi67/open-meteo
```

Then you can add this yaml file inside your `config/packages` directory (or you can copy it from the `config` directory of the package) :
```yaml
flibidi67_open_meteo:
  default:
    temperature_unit: "celcius" # or fahrenheit
    wind_speed_unit: "kmh" # or ms, mph, kn
    precipitation_unit: "mm" # or inch
    timeformat: "iso8601" #or unixtime
    timezone: "GMT" # auto, GMT or another existing timezone (eg: Europe/Paris).
    past_days: 0 # between 0 and 92
    forecast_days: 7 # between 0 and 16
    current_weather: false # true or false

  forecast:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    timezone: ~
    past_days: ~
    forecast_days: ~
    current_weather: ~

  historical:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    timezone: ~

  ecmwf:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    past_days: ~

  gfs:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    timezone: ~
    past_days: ~
    forecast_days: ~
    current_weather: ~

  meteofrance:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    timezone: ~
    past_days: ~
    current_weather: ~

  dwd:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    timezone: ~
    past_days: ~
    current_weather: ~

  jma:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    timezone: ~
    past_days: ~
    current_weather: ~

  metno:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    timezone: ~
    past_days: ~
    current_weather: ~

  gem:
    temperature_unit: ~
    wind_speed_unit: ~
    precipitation_unit: ~
    timeformat: ~
    timezone: ~
    past_days: ~
    current_weather: ~
```

You can either modify the values for a specific API's endpoint or just the default values. 
If a value is set to `null` for API's parameters, then the default value is taken.

## Usage
<span style="color:orange">/!\ To see all available parameters don't hesitate to take a look at the Open-Meteo Documentation and feel
free to explore the code</span>. 
### List of available services
 - ForecastService, to call the [Weather Forecast API](https://open-meteo.com/en/docs)
 - HistoricalService, to call the [Historical Weather API](https://open-meteo.com/en/docs/historical-weather-api)
 - ECMWFService, to call the [ECMWF Weather Forecast API](https://open-meteo.com/en/docs/ecmwf-api)
 - GFSService, to call the [GFS & HRRR Forecast API](https://open-meteo.com/en/docs/gfs-apiapi)
 - MeteoFranceService, to call the [MeteoFrance API](https://open-meteo.com/en/docs/meteofrance-api)
 - DWDService, to call the [DWD ICON API](https://open-meteo.com/en/docs/dwd-api)
 - JMAService, to call the [JMA API](https://open-meteo.com/en/docs/jma-api)
 - METNoService, to call the [MET Norway API](https://open-meteo.com/en/docs/metno-api)
 - GEMService, to call the [GEM API](https://open-meteo.com/en/docs/gem-api)

### Main service's methods
- getDaily() : Return the daily object.
- getHourly() : Return the hourly object.
- getSettings() : Return the settings object
- get() : Return the results
- setUrl(string $url): you can force the URL (need to be a full URL eg: https://api.open-meteo.com/v1/forecast?latitude=48.58&longitude=7.75&hourly=temperature_2m)

You can easily chain the different values you want to retrieve (except after retrieving the Settings). Autosuggest from your IDE will be you best friend :)

### Daily, Hourly and Minutely15
 - `with(string|array $parameter)` : $parameter can contain coma.
 - `get()`: call the service's `get()` method.
```php
$service->setCoordinates(48.58, 7.75)
        ->getHourly()
        ->with("temperature_2m,relativehumidity_2m,dewpoint_2m,cloudcover,cloudcover_low,cloudcover_mid");
```

### Use a formatter
In order to directly render the data returned by the OpenMeteo API in a desired format, you can set a formatter to use.
This package come with a `DefaultFormatter`. To use it :
```php
$myService->useDefaultFormatter()
```

The default formatter return the `hourly`, `daily` and `minutely_15` in a formatted associative array.
```php
[
    'hourly' => [
        '2023-03-22 00:00' => [
            'time' => DateTime // the datetime object corresponding to the key
            'temperature_2m' => 25.9 // for example
            // and all the other values you retrieve with the with function
        ]   
    ]
]
```

If you want to use your own formatter you should do like this
```php
namespace Your\Namespace;

use Flibidi67\OpenMeteo\Contract\FormatterInterface;

class MyAwesomeFormatter implements FormatterInterface {
    public function format(array $data): array {
        // do what you want with $data.
        return [];    
    }
}
// And then to use it
$myService->useFormatter(new MyAwesomeFormatter());
```

### Quick examples
```php
public function myFunction(ForecastService $forecastService) {
    $forecastService->setCoordinates(48.58, 7.75)
        ->getHourly()
        ->withApparentTemperature()
        ->withTemperature2m()
        ->getSettings()
        ->setCurrentWeather(true)
        ->getDaily()
        ->withApparentTemperatureMax();
        
    $results = $forecastService->get();
}

public function myFunction(ForecastService $forecastService) {
    $forecastService->setCoordinates(48.58, 7.75)
        ->getHourly()
        ->withAll(true) // true parameters is for precise that we want ALL the pressure level.
        ->getDaily()
        ->withAll();
        
    $results = $forecastService->get();
}

public function myFunction(ForecastService $forecastService) {
    $results = $forecastService->setCoordinates(48.58, 7.75)
        ->getHourly()
        ->withAll(true) // true parameters is for precise that we want ALL the pressure level.
        ->getDaily()
        ->withAll()
        ->get();
}

public function myFunction(HttpClientInterface $httpClient, ParameterBagInterface $parameterBag) {
    $forecastService = new ForecastService($httpClient, $parameterBag);
    $forecastService->setCoordinates(48.58, 7.75)->withHourlyTemperature2m();
}
```


