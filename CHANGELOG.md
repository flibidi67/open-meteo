# Changelog
## 1.0.0    

The first version of the package including : 
- Weather Forecast API's SDK
- Historical Weather API's SDK
- ECMWF Weather Forecast API's SDK
- GFS & HRRR Forecast API's SDK
- MeteoFrance API's SDK
- DWD ICON API's SDK
- JMA API's SDK
- MET Norway API's SDK
- GEM API's SDK
- usage of a custom formatter to format the data in a desired way
- configuration file to set settings

The package doesn't include the `models` API property.